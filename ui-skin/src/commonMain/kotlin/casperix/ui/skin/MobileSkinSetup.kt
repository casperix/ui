package casperix.ui.skin

import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.engine.Supplier
import casperix.ui.skin.component.MobileButtonSkinSetup
import casperix.ui.source.FontSource


/**
 * 	Experimental skin adaptation for mobile phones
 */
class MobileSkinSetup(supplier: Supplier, storage: HierarchicalStorage, fontSize: Int, customFontSource: FontSource?) {
	private val baseSkinSetup = BaseSkinSetup(supplier, storage, fontSize, customFontSource)
	private val mobileButtonSkinSetup = MobileButtonSkinSetup(supplier, storage, baseSkinSetup.buttonsSetup)
}