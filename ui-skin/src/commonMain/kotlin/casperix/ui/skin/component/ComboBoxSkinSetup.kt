package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.math.color.Color
import casperix.math.vector.Vector2d
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.combobox.ComboBoxSkin
import casperix.ui.component.text.TextShadowSettings
import casperix.ui.component.text.TextSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier

class ComboBoxSkinSetup(supplier: Supplier, storage: HierarchicalStorage, selectableSkinSetup: SelectableSkinSetup) {
	val icon = supplier.image(FileReference.classpath("graphics/combobox/icon.png"))



	val panelSkin = ComboBoxSkin(
		icon,
		selectableSkinSetup.selectableToggleSkin,
		selectableSkinSetup.itemButtonSkin,
		supplier.image9Slice(FileReference.classpath("graphics/combobox/shadow.png"), 20.0, -20.0)
	)

	init {
		storage.set(panelSkin)
	}

}