package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.DebugSkin
import casperix.ui.engine.Supplier

class DebugSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup) {
	val debugSkin = DebugSkin(
		supplier.image9Slice(FileReference.classpath("graphics/debug/self.png"), 1.0, 0.0),
		supplier.image9Slice(FileReference.classpath("graphics/debug/border.png"), 1.0, 0.0),
		supplier.image9Slice(FileReference.classpath("graphics/debug/gap.png"), 1.0, 0.0),
		supplier.image9Slice(FileReference.classpath("graphics/debug/children.png"), 1.0, 0.0),
		textSkinSetup.debugFont
	)

	init {
		storage.set(debugSkin)
	}
}