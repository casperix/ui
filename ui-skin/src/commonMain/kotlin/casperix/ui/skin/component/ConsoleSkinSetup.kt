package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.math.color.Color
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.console.ConsoleSkin
import casperix.ui.component.text.TextSkin
import casperix.ui.component.texteditor.TextEditorSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier

class ConsoleSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup, scrollerSkinSetup: ScrollerSkinSetup, inputSkinSetup: TextInputSkinSetup) {

	val consoleFont = supplier.fontFromFile(textSkinSetup.debugFontSource.file, textSkinSetup.defaultFontSize - 2, textSkinSetup.DEFAULT_CHARS)
	val consoleBack = supplier.image9Slice(FileReference.classpath("graphics/console/active.png"), 3.0)
	val consoleSkin = ConsoleSkin(
		TextEditorSkin(inputSkinSetup.inputSkin, TextSkin(textSkinSetup.defaultFont, textSkinSetup.textGap, Color.WHITE)),
		scrollerSkinSetup.scrollerSkin.copy(back = consoleBack),
		SideIndents(5),
		consoleFont,
		Color(200, 255, 200, 255),
		Color(255, 200, 200, 255),
		Color(240, 240, 240, 255),
	)

	init {
		storage.set(consoleSkin)
	}
}