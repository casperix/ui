package casperix.ui.skin.component

import casperix.math.color.Color
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.text.TextSkin
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier

class SelectableSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup, buttonsSkinSetup: ButtonsSkinSetup) {
	val labelDefault = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, color = Color.WHITE.setAlpha(0.8))
	val labelSelected = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, color = Color.WHITE.setAlpha(1.0))

	val selectableBorder = SideIndents(0, 0, 0, -1)
	val selectableButtonSkin = buttonsSkinSetup.create9SliceButtonSkin(
		supplier, "graphics/button/selectable/off/", 2.0, 0.0,
		pressedLabelCurrent = labelSelected,
		normalLabelCurrent = labelDefault
	).copy(gap = SideIndents.ZERO, border = selectableBorder)

	val itemButtonSkin = buttonsSkinSetup.create9SliceButtonSkin(
		supplier, "graphics/combobox/item/", 2.0, 0.0,
		pressedLabelCurrent = labelSelected,
		normalLabelCurrent = labelDefault
	).copy(gap = SideIndents.ZERO, border = selectableBorder)

	val selectableToggleSkin = ToggleSkin(
		buttonsSkinSetup.create9SliceButtonSkin(
			supplier, "graphics/button/selectable/on/", 2.0, 0.0,
			pressedLabelCurrent = labelSelected,
			normalLabelCurrent = labelDefault
		).copy(gap = SideIndents.ZERO, border = selectableBorder),
		buttonsSkinSetup.create9SliceButtonSkin(
			supplier, "graphics/button/selectable/off/", 2.0, 0.0,
			pressedLabelCurrent = labelSelected,
			normalLabelCurrent = labelDefault
		).copy(gap = SideIndents.ZERO, border = selectableBorder)
	)

	init {
		storage.set(ButtonSkin::class, selectableButtonSkin, "selectable")
		storage.set(ToggleSkin::class, selectableToggleSkin, "selectable")
	}
}