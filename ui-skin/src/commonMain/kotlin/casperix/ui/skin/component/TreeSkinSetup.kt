package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.button.ButtonDrawType
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.button.ButtonStateSkin
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.component.tree.TreeSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier

class TreeSkinSetup(supplier: Supplier, storage:HierarchicalStorage, textSkinSetup: TextSkinSetup) {
	val treeLeftArrow = ButtonStateSkin(supplier.image(FileReference.classpath("graphics/tree/off.png")), textSkinSetup.textSkin)
	val treeBottomArrow = ButtonStateSkin(supplier.image(FileReference.classpath("graphics/tree/on.png")), textSkinSetup.textSkin)
	val treeToggleSkin = ToggleSkin(
		ButtonSkin(true, SideIndents.ZERO, SideIndents(0), treeBottomArrow, treeBottomArrow, treeBottomArrow, treeBottomArrow, null, ButtonDrawType.SMOOTHED_LAYERS, SideIndents.ZERO, Vector2i(0, 1)),
		ButtonSkin(true, SideIndents.ZERO, SideIndents(0), treeLeftArrow, treeLeftArrow, treeLeftArrow, treeLeftArrow, null, ButtonDrawType.SMOOTHED_LAYERS, SideIndents.ZERO, Vector2i(0, 1)),
	)
	val treeSkin = TreeSkin(
		null,
		treeToggleSkin,
		32.0, Vector2d(16.0),
	)

	init {
		storage.set(treeSkin)
	}
}