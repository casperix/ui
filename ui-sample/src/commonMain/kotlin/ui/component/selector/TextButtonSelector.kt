package ui.component.selector

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.frame.FrameView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleGroup
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.util.BooleanGroupSelector

class TextButtonSelector<ItemClass>(val name: String, val buttonSize: Vector2d, val first: ItemClass, val items: List<Item<ItemClass>>, vertical: Boolean = true) : UIComponent(UINode()) {
	class Item<T>(val label: String, val value: T)

	val onSelect = StorageSignal(first)
	private val frame = FrameView()

	init {
		frame.title = name
		frame.content.layout = Layout.VERTICAL
		node += frame

		ToggleGroup.createAndPlace(frame.content, BooleanGroupSelector.Behaviour.ALWAYS_ONE, items.map { item ->
			ToggleView(item.label, buttonSize, item.value == first) { show ->
				if (show) {
					onSelect.value = item.value
				}
			}
		})
	}
}