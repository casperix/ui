package ui.component.selector

import casperix.math.vector.Vector2d
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutSide

object SideSelector : UIComponent(UINode(layout = Layout.VERTICAL)) {
	fun create(name:String, buttonSize: Vector2d, initial:LayoutSide): TextButtonSelector<LayoutSide> {
		return TextButtonSelector(name, buttonSize, initial, LayoutSide.values().map { TextButtonSelector.Item(getDirectionName(it), it) })
	}

	private fun getDirectionName(direction: LayoutSide): String {
		return when (direction) {
			LayoutSide.LEFT -> "left"
			LayoutSide.RIGHT -> "right"
			LayoutSide.TOP -> "top"
			LayoutSide.BOTTOM -> "bottom"
		}
	}
}