package ui.component

import casperix.math.vector.Vector2d
import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class Vector2dEditor(title: String, xName: String = "X", yName: String = "Y", initial: Vector2d = Vector2d.ZERO) : UIComponent(UINode()) {
	val onValue = StorageSignal<Vector2d>(initial)

	init {
		val editor = VectorEditor(title, listOf(xName, yName), listOf(initial.x, initial.y), node)
		editor.onValue.then {
			val x = it.getOrElse(0) { 0.0 }
			val y = it.getOrElse(1) { 0.0 }
			onValue.set(Vector2d(x, y))
		}
	}
}