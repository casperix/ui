package ui.sample

import casperix.app.window.WindowWatcher
import casperix.math.vector.Vector2d
import casperix.ui.component.VisibleLogic
import casperix.ui.component.button.ButtonView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.AbstractTab
import casperix.ui.component.tab.TabMenuView
import casperix.ui.component.text.TextView
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.engine.font.FontGenerator
import casperix.ui.layout.DividerLayout
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide
import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import ui.sample.demo.*
import ui.sample.demo.layout.UICollectionLayoutDemo

class App(watcher: WindowWatcher) {

	init {
		Gdx.gl.glClearColor(0.8f, 0.8f, 0.9f, 1f)
		val isMobile = Gdx.app.type == Application.ApplicationType.Android || Gdx.app.type == Application.ApplicationType.iOS
		val test = false
		FontGenerator.debugSaveImage = true

		val style = if (isMobile) {
			Style(
				28,
				Vector2d(120.0, 60.0),
				Vector2d(800.0, 600.0),
				Vector2d(60.0),
			)
		} else {
			Style(
				16,
				Vector2d(100.0, 40.0),
				Vector2d(800.0, 600.0),
				Vector2d(30.0),
			)
		}
		val environment = Environment(watcher, style)
		val clickSound = Gdx.audio.newSound(Gdx.files.internal("ui/click.ogg"))
		environment.root.events.onGlobalAction.then {
			if (it.key == "click") {
				clickSound.play(0.3f)
			}
		}

		if (test) {
			environment.engine.drawer.onDebugSelfArea.set(true)


			val A = ButtonView("header", Vector2d(200.0, 30.0), {})
			val B = ScrollBoxView()

			B.content += ButtonView("content", Vector2d(2000.0), {})
			DividerLayout(environment.root, A.node, B.node)
		} else {
			val menu = TabMenuView<AbstractTab>(LayoutSide.LEFT)
			environment.root += menu

			menu.tabs += listOf(
				UIConfig(environment).tab,
				UIConsoleDemo(environment).tab,
				UISceneDemo(environment).tab,
				UICollectionLayoutDemo(environment).tab,
				UIPlayground(environment).tab,
				UIImageDemo(environment).tab,
				UITabMenuDemo(environment).tab,
				UIChartDemo(environment).tab,
				UIDocumentDemo(environment).tab,
				UIScrollDemo(environment).tab,
				UIButtonDemo(environment).tab,
				UISpecialDemo(environment).tab,
				UIBarDemo(environment).tab,
				UITreeDemo(environment).tab,
				UITextEditorDemo(environment).tab,
			)
			menu.tabs[11].switch.set(true)
			createFps(environment)
		}
	}

	private fun createFps(environment: Environment) {
		val fpsInfo = TextView("")
		fpsInfo.node.events.nextFrame.then {
			fpsInfo.text = "fps: " + Gdx.graphics.framesPerSecond.toString()
		}
		fpsInfo.node.placement.sizeMode = SizeMode.const(Vector2d(100.0, 20.0))
		fpsInfo.node.placement.gap = SideIndents(10)
		fpsInfo.node.placement.align = LayoutAlign.RIGHT_TOP


		VisibleLogic(environment.root, fpsInfo.node, environment.onShowFps)
	}
}