package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.button.ButtonView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.TabMenuView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.type.LayoutSide
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import casperix.ui.util.BooleanGroupSelector
import ui.sample.Environment

class UITabMenuDemo(val environment: Environment) : UIComponent(UINode()) {
	val tab = ToggleTab(ToggleView("tabs", environment.style.buttonSize, StorageSignal(false)), UINode())

	init {
		val content = ScrollBoxView.viewport(tab.content, TableLayout(Orientation.HORIZONTAL, 2))

		content += createTabMenu(LayoutSide.LEFT)
		content += createTabMenu(LayoutSide.RIGHT)
		content += createTabMenu(LayoutSide.TOP)
		content += createTabMenu(LayoutSide.BOTTOM)
	}

	private fun createTabMenu(direction: LayoutSide): UINode {
		val tabMenu = TabMenuView<ToggleTab>(direction, BooleanGroupSelector.Behaviour.MAX_ONE, node = UINode(Vector2d(400.0)))
		val max = if (direction == LayoutSide.LEFT) 8 else 4

		for (i in 1..max) {
			tabMenu.tabs += ToggleTab(ToggleView("tab$i", Vector2d(80.0, 40.0), false, {}), createContent(i))
		}
		return tabMenu.node
	}

	private fun createContent(i: Int): UINode {
		val content = UINode(Vector2d(120.0, 120.0))
		content += ButtonView("button $i", Vector2d(80.0, 40.0), {})
		return content
	}


}