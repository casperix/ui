package ui.sample.demo

import casperix.gdx.loader.AssetLoader
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.graphic.GdxScene
import com.badlogic.gdx.graphics.g3d.ModelInstance
import ui.sample.Environment

class UISceneDemo(val environment: Environment) {

	val tab = ToggleTab(ToggleView("scene", environment.style.buttonSize, StorageSignal(false)), UINode())
	val scene = GdxScene(UINode())


	init {
		tab.content += scene

		scene.camera.position.set(32f, 32f, 32f)
		scene.camera.lookAt(0f, 0f, 0f)
		scene.camera.update()

		val model = AssetLoader.loadModel("model/oil_well.g3db")
		(0 until  16).forEach { x->
			(0 until 16).forEach { z->
				val instance = ModelInstance(model, x * 4f, 0f, z * 4f)
				instance.transform.scale(.1f, .1f, .1f)
				scene.instances += instance
			}
		}
	}

}