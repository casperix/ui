package ui.sample.demo

import casperix.signals.concrete.StorageSignal
import casperix.ui.component.console.ConsoleView
import casperix.ui.component.console.ConsoleCommand
import casperix.ui.component.console.ConsoleLogic
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import ui.sample.Environment

class UIConsoleDemo(val environment: Environment) {
	val tab = ToggleTab(ToggleView("console", environment.style.buttonSize, StorageSignal(false)), UINode())

	init {
		val logic = ConsoleLogic()
		logic.commands += ConsoleCommand("Print", "") {
			it.joinToString("\n")
		}
		logic.commands += ConsoleCommand("Some", "") {
			"Some output"
		}
		logic.commands += ConsoleCommand("Other", "") {
			"Other output"
		}
		logic.commands += ConsoleCommand("Lines", "") {
			val amount = it.first().toInt()
			(0 until amount).joinToString("\n")
		}

		val console = ConsoleView(logic)
		tab.content += console

		tab.toggle.switch.then {
			if (it) {
				console.makeInputFocus()
			}
		}

		logic.execute(ConsoleLogic.CommandInfo(logic.commands.last(), listOf("100")))
	}

}