package ui.sample.demo

import casperix.signals.concrete.StorageSignal
import casperix.ui.component.document.DocumentView
import casperix.ui.component.document.DocumentSkin
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import ui.sample.Environment

class UIDocumentDemo(val environment: Environment) {
	val tab = ToggleTab(ToggleView("docs", environment.style.buttonSize, StorageSignal(false)), UINode(name = "doc"))

	init {
		val content = tab.content

		val document = DocumentView("It is \"Document\"\n")
		document.text += "Color: [color:FF0000:test/].\n"
		document.text += "Text size: [size:36:test/]\n"
		document.text += "Clickable: [action:someaction:click for someaction]\n"
		document.text += "Text font: [font:fonts/Verdana.ttf:OTHER FONT/]\n"
		document.text += " Small icon: [image:icon.png]\n"
		document.text += " Resized image: [image:test.png:128:64]\n"

		document.text += DocumentSource.text
		content += document

		environment.root.properties.get(DocumentSkin::class)?.let { documentSkin ->
			documentSkin.customActions["new line"] = { document.text += "\nnew line added" }
		}
	}

}