package ui.sample.demo

object DocumentSource {

	private val enText =
		"The graphical user interface (GUI /dʒiːjuːˈaɪ/ jee-you-eye[1][Note 1] or /ˈɡuːi/[2]) is a form of user interface that allows users to interact with electronic devices through graphical icons and audio indicator such as primary notation, instead of text-based user interfaces, typed command labels or text navigation. GUIs were introduced in reaction to the perceived steep learning curve of command-line interfaces (CLIs),[3][4][5] which require commands to be typed on a computer keyboard. "
	private val ruText = "Чаще всего элементы интерфейса в GUI реализованы на основе метафор и отображают их назначение и свойства, что облегчает понимание и использование электронных устройств неподготовленными пользователями. "
	private val frText =
		"Ce type d'interface a été créé en 1973 sur le Xerox Alto par les ingénieurs du Xerox PARC pour remplacer les interfaces en ligne de commande. Mis sur le marché à la fin des années 1970 avec le Star de Xerox, le Lisa d'Apple et popularisé par cette dernière firme avec l'ordinateur Macintosh, commercialisé en 1984"

	private val zhText = "古代华夏族發源黃河流域的中原地区[1]，以為居天下之中[註 1]，故稱其地為中國。後各朝疆土漸廣，凡所轄境皆稱為中國[有歧义]，亦稱中華。[3][4][5][6] 現今國際上廣泛承認代表「中國」的政權是中华人民共和国"

	private val arText = "في أغلب الأحيان ، يتم تنفيذ عناصر الواجهة في واجهة المستخدم الرسومية على أساس الاستعارات وتعكس الغرض منها وخصائصها ، مما يسهل على المستخدمين غير المستعدين فهم واستخدام الأجهزة الإلكترونية."

	val text = 			"\n\nenglish:\n" + enText + "\n\nrussian:\n" + ruText + "\n\nfrench:\n" + frText + "\n\nchinese (partially supported):\n" + zhText + "\n\narabic (not supported):\n" + arText


}