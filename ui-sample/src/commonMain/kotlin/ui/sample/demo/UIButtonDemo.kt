package ui.sample.demo

import casperix.file.FileReference
import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.button.ButtonView
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.checkbox.CheckBoxView
import casperix.ui.component.frame.FrameView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import casperix.ui.type.LayoutAlign
import ui.sample.Environment

class UIButtonDemo(val environment: Environment) {
	val frameSize = UIHelper.FRAME_SIZE
	val tab = ToggleTab(ToggleView("button", environment.style.buttonSize, StorageSignal(false)), UINode())
	private val info = TextView("")

	init {
		tab.toggle.switch.then {
			if (it) {
				tab.content.children.clear()
				create()
			}
		}
	}

	fun create() {
		val content = ScrollBoxView.viewport(tab.content, TableLayout(Orientation.VERTICAL, 4))

		val bitmap = environment.engine.supplier.bitmapFromFile(FileReference.classpath("ui/smallIcon.png"))

		content += FrameView(frameSize, "output", info)

		content += FrameView(
			frameSize,
			"text button",
			ButtonView("button", Vector2d(160.0, 80.0)) {
				info.text = "text button pressed"
			}.node
		)

		content += FrameView(
			frameSize,
			"toggle button",
			ToggleView("toggle", Vector2d(80.0), false) {
				info.text = "toggle is $it"
			}.node
		)
		content += FrameView(
			frameSize,
			"toggle button with image",
			ToggleView("toggle", Vector2d(100.0), false) {
				info.text = "toggle is $it"
			}.apply {
				addBitmap(bitmap)
			}.node
		)

		content += FrameView(
			frameSize,
			"image button",
			ButtonView(bitmap, Vector2d(160.0, 120.0)) {
				info.text = "image button pressed"
			}.node
		)


		content += FrameView(
			frameSize,
			"text and image button",
			ButtonView("label", bitmap, Vector2d(160.0, 120.0)) {
				info.text = "image button pressed"
			}.node
		)

		val frame2 = FrameView(frameSize, "intersected toggles", UINode())

		val node1 = ToggleView("outside", Vector2d(220.0, 100.0), false).node
		node1.name = "outside"
		node1.placement.align = LayoutAlign.CENTER_TOP
		frame2.content += node1

		val node2 = ToggleView("inside", Vector2d(120.0, 160.0), false).node
		node2.name = "inside"
		node2.placement.align = LayoutAlign.CENTER_BOTTOM
		frame2.content += node2

		content += frame2

		content += FrameView(frameSize, "simple checkbox", CheckBoxView(null, Vector2d(40.0), true) {
			info.text = "checkbox is $it"
		})

		content += FrameView(frameSize, "check box with text", CheckBoxView("selectable text", Vector2d(40.0), false) {
			info.text = "labeled checkbox is $it"
		})

		content += FrameView(
			frameSize,
			"few buttons",
			UINode().apply {
				layout = Layout.VERTICAL
				this += ButtonView("A", Vector2d(80.0, 70.0), {})
				this += ButtonView("B", Vector2d(80.0, 70.0), {})
				this += ButtonView("C", Vector2d(80.0, 70.0), {})
			}
		)

		content += FrameView(
			frameSize,
			"colored button",
			UINode().apply {
				layout = Layout.VERTICAL
				this += ButtonView("Danger", Vector2d(80.0, 70.0), {}).apply {
					val red = environment.root.properties.get(ButtonSkin::class, "red")
					skin = red
				}
				this += ButtonView("Cancel", Vector2d(80.0, 70.0), {})
			}
		)
	}

}