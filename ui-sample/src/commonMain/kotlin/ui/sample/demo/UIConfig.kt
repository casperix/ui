package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.checkbox.CheckBoxView
import casperix.ui.component.frame.FrameView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.engine.SceneDrawer
import casperix.ui.layout.Layout
import casperix.ui.skin.BaseSkinSetup
import casperix.ui.skin.MobileSkinSetup
import casperix.ui.type.LayoutAlign
import com.badlogic.gdx.Gdx
import ui.component.SideIndentsEditor
import ui.component.Vector2dEditor
import ui.sample.Environment

class UIConfig(val environment: Environment) {
	val tab = ToggleTab(ToggleView("config", environment.style.buttonSize, StorageSignal(false)), UINode())

	private val checkerSize = environment.style.checkerSize

	private val borderEditor = SideIndentsEditor("border", initial = SideIndents(6))
	private val gapEditor = Vector2dEditor("gap", initial = Vector2d(5.0))

	init {
//		val content = Scroller.viewport(tab.content, Layout.BOTTOM)
		tab.content += createControlPanel()
	}

	private fun createControlPanel(): UINode {
		val bar = UINode()
		bar.placement.align = LayoutAlign.LEFT
		bar.placement.sizeMode = SizeMode.constWidth(300.0)
		bar.layout = Layout.VERTICAL

		val miscFrame = FrameView("other", Layout.VERTICAL)
		miscFrame.content += CheckBoxView("show FPS", checkerSize, false) {
			environment.onShowFps.set(it)
		}
		miscFrame.content += CheckBoxView("filter over screen", checkerSize, true) {
			SceneDrawer.filterOverScreenElements = it
		}
		miscFrame.content += CheckBoxView("V-sync", checkerSize, true) {
			Gdx.graphics.setVSync(it)
		}
		bar += miscFrame

		bar += createDebugFrame()

		borderEditor.node.placement.align = LayoutAlign.LEFT_TOP
		bar += borderEditor

		gapEditor.node.placement.align = LayoutAlign.LEFT_TOP
		bar += gapEditor
		return bar
	}

	private fun createDebugFrame(): UIComponent {
		val debugFrame = FrameView("ui debug", Layout.BOTTOM)
		val content = debugFrame.content

		content += CheckBoxView("name", checkerSize, false) {
			environment.engine.drawer.onDebugName.set(it)
		}

		content += CheckBoxView("mobile-skin", checkerSize, false) {
			if (it) {
				MobileSkinSetup(environment.engine.supplier, environment.root.properties, environment.style.fontSize, null)
			} else {
				BaseSkinSetup(environment.engine.supplier, environment.root.properties, environment.style.fontSize, null)
			}
		}

		content += CheckBoxView("self area", checkerSize, false) {
			environment.engine.drawer.onDebugSelfArea.set(it)
		}

		content += CheckBoxView("children area", checkerSize, false) {
			environment.engine.drawer.onDebugChildrenArea.set(it)
		}

		content += CheckBoxView("self border", checkerSize, false) {
			environment.engine.drawer.onDebugSelfBorder.set(it)
		}

		content += CheckBoxView("self gap", checkerSize, false) {
			environment.engine.drawer.onDebugSelfBorder.set(it)
		}

		content += CheckBoxView("continuous rendering", checkerSize, Gdx.graphics.isContinuousRendering) {
			environment.engine.on1FPSMode.set(!it)
		}

		content += CheckBoxView("debug input", checkerSize, false) {
			environment.engine.onDebugInput.set(it)
		}

		return debugFrame
	}

}