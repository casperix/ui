package ui.sample.demo

import casperix.misc.clamp
import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.signals.concrete.StorageSignal
import casperix.math.color.Color
import casperix.math.interpolation.*
import casperix.ui.component.chart.*
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.type.LayoutSide
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import com.badlogic.gdx.Gdx
import ui.sample.Environment
import kotlin.random.Random


class LineChartGenerator(seed: Int) {
	val r = Random(seed)
	val points = mutableListOf(Vector2d(0.0, 30.0))

	fun next(): LineChartSource {
		val last = points.last()
		points += Vector2d(last.x + 1.0, (last.y + r.nextDouble(-1.0, 1.0)).clamp(0.0, 60.0))
		if (points.size > 200) points.removeAt(0)

		return LineChartSource(points)
	}
}

class FPSDiagramGenerator() {
	val points = mutableListOf(Vector2d(0.0, 0.0))

	fun next(): LineChartSource {
		val last = points.last()
		val fps = Gdx.graphics.framesPerSecond.toDouble()
		points += Vector2d(last.x + 1.0, fps.clamp(0.0, 60.0))
		if (points.size > 200) points.removeAt(0)

		return LineChartSource(points)
	}
}

class UIChartDemo(val environment: Environment) {
	val tab = ToggleTab(ToggleView("chart", environment.style.buttonSize, StorageSignal(false)), UINode())

	val genA = LineChartGenerator(1)
	val genB = FPSDiagramGenerator()
	val dynamicChart = ChartsView(node = UINode(Vector2d(600.0, 300.0)))

	init {
		val content = ScrollBoxView.viewport(tab.content, TableLayout(Orientation.VERTICAL, 2))

		val defaultArea = Box2d(Vector2d(0.0, 0.0), Vector2d(1.0, 1.0))
		val bigArea = Box2d(Vector2d(0.0, -0.25), Vector2d(1.0, 1.25))
		val pointAmount = 100

		content +=UIHelper. buildGraphics(
			"", pointAmount, false, defaultArea, listOf(
				ChartInfo("linear", { linearInterpolate(0.0, 1.0, it) }, Color.RED),
				ChartInfo("cosine", { cosineInterpolate(0.0, 1.0, it) }, Color.GREEN),
				ChartInfo("hermite", { hermiteInterpolate(0.0, 1.0, it) }, Color.YELLOW),
			)
		)

		content +=UIHelper. buildGraphics(
			"ease quadratic", pointAmount, false, defaultArea, listOf(
				ChartInfo("In", { easeInQuad(0.0, 1.0, it) }, Color.RED),
				ChartInfo("Out", { easeOutQuad(0.0, 1.0, it) }, Color.GREEN),
				ChartInfo("InOut", { easeInOutQuad(0.0, 1.0, it) }, Color.YELLOW),
			)
		)

		content +=UIHelper. buildGraphics(
			"ease cubic", pointAmount, false, defaultArea, listOf(
				ChartInfo("Int", { easeInQuad(0.0, 1.0, it) }, Color.RED),
				ChartInfo("Out", { easeOutQuad(0.0, 1.0, it) }, Color.GREEN),
				ChartInfo("InOut", { easeInOutQuad(0.0, 1.0, it) }, Color.YELLOW),
			)
		)

		content += UIHelper.buildGraphics(
			"ease elastic", pointAmount, false, bigArea, listOf(
				ChartInfo("InOut", { easeInOutElastic(0.0, 1.0, it) }, Color.RED),
			)
		)

		content += dynamicChart

		content += UIHelper. buildGraphics(
			"point only", 10, true, bigArea, listOf(
				ChartInfo("InOut", { easeInOutQuad(0.0, 1.0, it) }, Color.RED),
			)
		)

		dynamicChart.node.events.nextFrame.then {
			val diagramA = genA.next()
			val diagramB = genB.next()

			val minX = diagramA.points.first().x
			val maxX = diagramA.points.last().x


			dynamicChart.settings = ChartBoxSettings(
				name = "dynamic charts",
				legendSide = LayoutSide.LEFT,
				sourceArea = Box2d(Vector2d(minX, 0.0), Vector2d(maxX, 60.0)),
				xAxis = ChartAxisSettings("frames", 50.0, 8.0, 0, ChartLineSettings(Color.RED.toColor4d()), textColor = Color.BLACK.toColor4d()),
				yAxis = ChartAxisSettings("FPS", 20.0, 8.0, 0, ChartLineSettings(Color.GREEN.toColor4d()), textColor = Color.BLACK.toColor4d()),
			)

			dynamicChart.clearAllCharts()
			dynamicChart.addChart("random", ChartSettings(ChartLineSettings(Color.YELLOW.toColor4d(), 2.0), null), diagramA)
			dynamicChart.addChart("fps", ChartSettings(ChartLineSettings(Color.BLUE.toColor4d(), 2.0), null), diagramB)
		}
	}

	class ChartInfo(val name: String, val func: (Double) -> Double, val color: Color)

	companion object {


	}
}