package ui.sample.demo.layout

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.tab.AbstractTab
import casperix.ui.component.tab.TabMenuView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.SideIndents
import casperix.ui.core.UINode
import casperix.ui.layout.*
import casperix.ui.type.LayoutSide
import ui.sample.Environment
import kotlin.random.Random

class UICollectionLayoutDemo(val environment: Environment) {
	private val tabSize = environment.style.buttonSize

	val tab = ToggleTab(ToggleView("layout", tabSize, StorageSignal(false)), UINode())

	private val layoutSelector = TabMenuView<AbstractTab>(LayoutSide.LEFT)

	private val defaultGap = SideIndents(0.0)
	private val defaultBorder = SideIndents(0.0)

	private val orientationDemo = UILayoutDemo(environment, LayoutSettings(6, defaultBorder, defaultGap, layoutSide = LayoutSide.LEFT), ::setupOrientation)
	private val tableDemo = UILayoutDemo(environment, LayoutSettings(6, defaultBorder, defaultGap, rowOrientation = Orientation.HORIZONTAL, rowLength = 3), ::setupTable)
	private val dividerDemo = UILayoutDemo(environment, LayoutSettings(2, defaultBorder, defaultGap, layoutSide = LayoutSide.LEFT), ::setupDivider)
	private val screenDemo = UILayoutDemo(environment, LayoutSettings(1, defaultBorder, defaultGap), ::setupScreen)
	private val nullDemo = UILayoutDemo(environment, LayoutSettings(6, defaultBorder, defaultGap), ::setupNull)

	init {
		layoutSelector.tabs += ToggleTab(ToggleView("orientation", tabSize), orientationDemo.node)
		layoutSelector.tabs += ToggleTab(ToggleView("table", tabSize), tableDemo.node)
		layoutSelector.tabs += ToggleTab(ToggleView("divider", tabSize), dividerDemo.node)
		layoutSelector.tabs += ToggleTab(ToggleView("screen", tabSize), screenDemo.node)
		layoutSelector.tabs += ToggleTab(ToggleView("null", tabSize), nullDemo.node)
		tab.content += layoutSelector
	}


	private fun setupOrientation(node: UINode, settings: LayoutSettings) {
		node.layout = LineLayout(settings.layoutSide ?: LayoutSide.LEFT)
	}

	private fun setupTable(node: UINode, settings: LayoutSettings) {
		node.layout = TableLayout(settings.rowOrientation ?: Orientation.HORIZONTAL, settings.rowLength ?: 1)
	}

	private fun setupNull(node: UINode, settings: LayoutSettings) {
		val random = Random(settings.randomId)
		val sizeList = listOf(Vector2d(120.0, 120.0), Vector2d(140.0, 80.0), Vector2d(80.0, 100.0))

		node.layout = null
		node.clippingContent = false
		node.children.forEach {
			it.placement.setArea(Vector2d(random.nextDouble() * 300.0, random.nextDouble() * 300.0) - (it.placement.size / 2.0), sizeList.random(random))
		}
	}

	private fun setupDivider(node: UINode, settings: LayoutSettings) {
		node.layout = DividerLayout(settings.layoutSide ?: LayoutSide.LEFT)
	}

	private fun setupScreen(node: UINode, settings: LayoutSettings) {
		node.layout = Layout.SCREEN
	}


}