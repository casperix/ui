package ui.sample.demo

import casperix.math.axis_aligned.Box2d
import casperix.math.color.Color
import casperix.math.geometry.Triangle2d
import casperix.math.vector.Vector2d
import casperix.math.vector.toQuad
import casperix.ui.component.chart.*
import casperix.ui.core.UINode
import casperix.ui.engine.Shape
import casperix.ui.graphic.ShapeBuilder
import casperix.ui.graphic.ShapeStyle
import casperix.ui.type.LayoutSide
import kotlin.random.Random

object UIHelper {
	val FRAME_SIZE = Vector2d(250.0, 250.0)
	val random = Random(0)

	fun createRandomShape(): Shape {
		val color = Color.COLORS.random(random).toColor4d()
		val type = random.nextInt(0, 3)

		if (type == 0) {
			return ShapeBuilder.rectangle(Vector2d(0.0), Vector2d(200.0, 100.0), ShapeStyle(color))
		} else if (type == 1) {
			val q = Box2d.byCorners(Vector2d(0.0), Vector2d(200.0, 100.0)).toQuad()

			return ShapeBuilder.triangle(Triangle2d(q.v0, q.v1, q.v2), ShapeStyle(color))

		} else {
			return ShapeBuilder.ellipse(color, Vector2d.ZERO, 100.0, 50.0, ShapeStyle(color))
		}
	}


	fun buildGraphics(name: String, pointAmount: Int, showPoint: Boolean, sourceArea: Box2d, infos: Collection<UIChartDemo.ChartInfo>, preferredSize: Vector2d = Vector2d(450.0, 300.0)): ChartsView {
		val chart = ChartsView(node = UINode(preferredSize))

		infos.forEach { info ->
			val points = (0..pointAmount).map { index ->
				val x = index.toDouble() / pointAmount.toDouble()
				val y = info.func(x)
				Vector2d(x, y)
			}

			chart.settings = ChartBoxSettings(
				name = name,
				axisLabelSize = Vector2d(50.0, 35.0),
				legendSide = LayoutSide.LEFT,
				sourceArea = sourceArea,
				xAxis = ChartAxisSettings("t", 0.5, 8.0, 1, ChartLineSettings(Color.BLUE.toColor4d())),
				yAxis = ChartAxisSettings("x", 0.5, 8.0, 1, ChartLineSettings(Color.BLUE.toColor4d())),
			)
			val settings = if (showPoint) {
				ChartSettings(null, ChartPointSettings(info.color.toColor4d()))
			} else {
				ChartSettings(ChartLineSettings(info.color.toColor4d()), null)
			}
			chart.addChart(info.name, settings, LineChartSource(points))
		}
		return chart
	}
}