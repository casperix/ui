package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.frame.FrameView
import casperix.ui.component.progress.ProgressBarView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import casperix.ui.type.LayoutSide
import ui.sample.Environment

class UIBarDemo(val environment: Environment) {
	val frameSize = UIHelper.FRAME_SIZE
	val tab = ToggleTab(ToggleView("bar", environment.style.buttonSize, StorageSignal(false)), UINode())
	private val info = TextView("")

	init {
		tab.toggle.switch.then {
			if (it) {
				tab.content.children.clear()
				create()
			}
		}
	}

	private fun create() {
		val content = ScrollBoxView.viewport(tab.content, TableLayout(Orientation.VERTICAL, 4))

		content += FrameView(frameSize, "output", info)

		LayoutSide.values().forEach { side ->
			val orientation = side.orientation()
			val size = if (orientation == Orientation.HORIZONTAL) Vector2d(160.0, 40.0) else Vector2d(40.0, 160.0)

			content += FrameView(
				frameSize,
				" ${side.name} bar",
				ProgressBarView(size, side).apply {
					barAnimate(this)
				}.node
			)
		}

		content += FrameView(
			frameSize,
			"multi bar",
			UINode(layout = Layout.HORIZONTAL).apply {
				this += ProgressBarView(Vector2d(30.0, 80.0), LayoutSide.BOTTOM).apply {
					barAnimate(this, 0.1)
				}.node
				this += ProgressBarView(Vector2d(30.0, 80.0), LayoutSide.BOTTOM).apply {
					barAnimate(this, 0.15)
				}.node
				this += ProgressBarView(Vector2d(30.0, 80.0), LayoutSide.BOTTOM).apply {
					barAnimate(this, 0.25)
				}.node
			}
		)
	}

	private fun barAnimate(bar: ProgressBarView, speed:Double = 0.1) {
		bar.node.events.nextFrame.then {
			bar.onValue.value += speed * it
			if (bar.onValue.value > 1.0) bar.onValue.value -= 1.0
		}
	}

}