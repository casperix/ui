package ui.sample

import casperix.app.window.WindowWatcher
import casperix.signals.concrete.StorageSignal
import casperix.ui.engine.GdxUIManager

class Environment(val watcher: WindowWatcher, val style: Style) {
	val engine = GdxUIManager(watcher, true, true)
	val root = engine.root
	val assets = UIAssets(engine.supplier)

	val onShowFps = StorageSignal(false)
}