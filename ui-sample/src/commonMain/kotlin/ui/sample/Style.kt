package ui.sample

import casperix.math.vector.Vector2d

class Style (
	val fontSize:Int,
	val buttonSize:Vector2d,
	val contentSize: Vector2d,
	val checkerSize: Vector2d,
)