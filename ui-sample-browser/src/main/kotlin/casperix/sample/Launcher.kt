package casperix.sample

import ui.sample.App

fun main(args: Array<String>) {
	GdxDesktopApplicationLauncher(GdxApplicationConfig("UI Demo", "icon.png")) {
		App(it)
	}
}

