package casperix.ui.engine.font

import casperix.file.FileReference
import casperix.misc.Disposable
import casperix.ui.engine.toGdxFile
import casperix.ui.source.FontLink
import casperix.ui.source.FontMultiLink
import casperix.ui.source.FontSource
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.PixmapPacker
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import java.util.Locale

class FontGenerator(val yDown: Boolean) : Disposable {
	companion object {
		private var buildIteration = 0
		var debugSaveImage = false
		var debugDrawBorder = false
	}

	data class FontEntry(val source: FontSource, val parameters: FreeTypeFontGenerator.FreeTypeFontParameter, val bitmapFontData: FreeTypeFontGenerator.FreeTypeBitmapFontData)

	private val entryList = mutableMapOf<FontSource, FontEntry>()
	private val generatorList = mutableMapOf<FileReference, FreeTypeFontGenerator>()

	fun buildBitmapFont(fontSource: FontSource, characters: Set<Char>): BitmapFont {
		val fontLinks = if (fontSource is FontLink) {
			listOf(fontSource)
		} else if (fontSource is FontMultiLink) {
			fontSource.links
		} else {
			throw Error("Unsupported font source: $fontSource")
		}

		val entry = entryList.getOrPut(fontSource) {
			val parameters = FreeTypeFontGenerator.FreeTypeFontParameter()
			parameters.characters = ""
			parameters.flip = yDown
			parameters.gamma = 1f
			parameters.renderCount = 1
			if (debugDrawBorder) {
				parameters.borderWidth = 2f
				parameters.borderColor = Color.BLACK
			} else {
				parameters.borderWidth = 0f
			}
			parameters.incremental = true
			//	TODO: define size, for all glyphs
			parameters.packer = PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 1, false)

			FontEntry(fontSource, parameters, FreeTypeFontGenerator.FreeTypeBitmapFontData())
		}

		var remainCharacters = characters.toList()
		entry.parameters.characters = remainCharacters.joinToString("")

		val bitmapFontList = fontLinks.map { fontLink ->
			entry.parameters.size = fontLink.size
			entry.parameters.kerning = true
			entry.parameters.characters = remainCharacters.joinToString("")

			val generator = generatorList.getOrPut(fontLink.file) {
				val gdxFile = fontLink.file.toGdxFile()
				FreeTypeFontGenerator(gdxFile)
			}
			val bitmapFont = generator.generateFont(entry.parameters, entry.bitmapFontData)

			remainCharacters = remainCharacters.filter {
				bitmapFont.data.getGlyph(it) == bitmapFont.data.missingGlyph
			}
			bitmapFont
		}

		//	HACK, for use first font as default
		val firstFontLink = fontLinks.first()
		val gdxFile = firstFontLink.file.toGdxFile()
		entry.parameters.characters = ""
		entry.parameters.size = firstFontLink.size
		FreeTypeFontGenerator(gdxFile).generateFont(entry.parameters, entry.bitmapFontData)

		if (debugSaveImage) {
			buildIteration++

			bitmapFontList.forEach { bitmapFont ->
				val pixmap = bitmapFont.region.texture.textureData.consumePixmap()
				val pixmapPath = "temp/" + bitmapFont.data.name + "_$buildIteration.png"
				PixmapIO.writePNG(FileHandle(pixmapPath), pixmap)
			}
		}
		return bitmapFontList.last()
	}

	override fun dispose() {
		entryList.values.forEach {
			it.bitmapFontData.dispose()
		}
		entryList.clear()
	}
}