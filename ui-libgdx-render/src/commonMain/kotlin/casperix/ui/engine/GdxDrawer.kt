package casperix.ui.engine

import casperix.map2d.IntMap2D
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.math.axis_aligned.Box2i
import casperix.math.geometry.Quad2d
import casperix.math.geometry.Triangle2d
import casperix.signals.concrete.StorageSignal
import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.ui.component.DebugSkin
import casperix.ui.core.UINode
import casperix.ui.engine.font.GdxFont
import casperix.ui.font.Font
import casperix.ui.graphic.TextGraphic
import casperix.ui.layout.Align
import casperix.ui.type.LayoutAlign
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.utils.viewport.ScreenViewport


class GdxDrawer(val yDown: Boolean, val supplier: GdxSupplier) : Drawer {

	override val onViewport = StorageSignal(Vector2i(Gdx.graphics.width, Gdx.graphics.height))
	val onDebugSelfArea = StorageSignal(false)
	val onDebugChildrenArea = StorageSignal(false)
	val onDebugSelfBorder = StorageSignal(false)
	val onDebugSelfGap = StorageSignal(false)
	val onDebugName = StorageSignal(false)

	private val camera = OrthographicCamera()
	private val viewport = ScreenViewport(camera)
	private var area: Box2i? = null
	private var lastColor:Color4d? = null

	val batch = PolygonSpriteBatch(1000, 1000, null)
	private val blankTexture = supplier.bitmapFromPixels(IntMap2D(Vector2i.ONE, IntArray(1) { Color.WHITE.value }))


	init {
		camera.setToOrtho(yDown)
		batch.enableBlending()
		batch.setBlendFunction(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA)
	}

	override fun getScissor(): Box2i? {
		return area
	}

	override fun setScissor(area: Box2i?) {
		if (this.area == area) return

		if (batch.isDrawing) {
			batch.end()
			batch.begin()
		}

		if (area == null) {
			this.area = null
			Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST)
		} else {
			val viewportMax = Vector2i(viewport.screenWidth, viewport.screenHeight) - Vector2i.ONE
			val clamped = area//Box2i(area.min.clamp(Vector2i.ZERO, viewportMax), area.max.lower(viewportMax))

			Gdx.gl.glScissor(clamped.min.x, viewport.screenHeight - clamped.max.y - 1, clamped.dimension.x, clamped.dimension.y)
			Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST)
			this.area = clamped
		}
	}


	override fun drawTransformedBitmap(bitmap: Bitmap, targetArea: Quad2d, color: Color4d) {
		val deviceImage = bitmap as TextureContainer
		val textureRegion = TextureRegion(deviceImage.texture, 0, 0, 1, 1)

		val vertices = floatArrayOf(
			targetArea.v0.x.toFloat(), targetArea.v0.y.toFloat(),
			targetArea.v1.x.toFloat(), targetArea.v1.y.toFloat(),
			targetArea.v2.x.toFloat(), targetArea.v2.y.toFloat(),
			targetArea.v3.x.toFloat(), targetArea.v3.y.toFloat(),
		)
		val indices = shortArrayOf(0, 1, 2, 0, 2, 3)
		val region = PolygonRegion(textureRegion, vertices, indices)

		val sourcePosition = bitmap.region.min
		val sourceSize = bitmap.region.dimension

		val u1 = sourcePosition.x.toFloat() / sourceSize.x.toFloat()
		val u2 = (sourcePosition.x + sourceSize.x - 1).toFloat() / sourceSize.x.toFloat()
		val v1 = sourcePosition.y.toFloat() / sourceSize.y.toFloat()
		val v2 = (sourcePosition.y + sourceSize.y - 1).toFloat() / sourceSize.y.toFloat()

		region.textureCoords[0] = u1; region.textureCoords[1] = v1
		region.textureCoords[2] = u2; region.textureCoords[3] = v1
		region.textureCoords[4] = u2; region.textureCoords[5] = v2
		region.textureCoords[6] = u1; region.textureCoords[7] = v2

		setColor(color)
		batch.draw(region, 0f, 0f)
	}

	override fun drawBitmap(bitmap: Bitmap, targetPosition: Vector2d, targetSize: Vector2d, sourcePosition: Vector2i, sourceSize: Vector2i, color: Color4d) {
		val deviceImage = bitmap as TextureContainer
		setColor(color)
		batch.draw(
			deviceImage.texture,
			targetPosition.x.toFloat(),
			targetPosition.y.toFloat(),
			targetSize.x.toFloat(),
			targetSize.y.toFloat(),
			sourcePosition.x,
			sourcePosition.y,
			sourceSize.x,
			sourceSize.y,
			false,
			yDown,
		)
	}

	override fun drawTriangle(targetArea: Triangle2d, color: Color4d) {
		setColor(color)
		val region = PolygonRegion(TextureRegion(blankTexture.texture), quadToFloatVertices(targetArea), shortArrayOf(0, 1, 2))
		batch.draw(region, 0f, 0f)
	}

	private fun setColor(color: Color4d) {
		batch.setColor(color.x.toFloat(), color.y.toFloat(), color.z.toFloat(), color.w.toFloat())
	}

	private fun quadToFloatVertices(self: Triangle2d): FloatArray {
		return floatArrayOf(
			self.v0.x.toFloat(), self.v0.y.toFloat(),
			self.v1.x.toFloat(), self.v1.y.toFloat(),
			self.v2.x.toFloat(), self.v2.y.toFloat(),
		)
	}

	fun draw(root: UINode) {
		val skin = root.properties.get(DebugSkin::class)!!
		val debugMode = DebugMode(skin, onDebugSelfArea.value, onDebugSelfBorder.value, onDebugSelfGap.value, onDebugChildrenArea.value, onDebugName.value)

		batch.begin()
		SceneDrawer.draw(this, root, debugMode)
		batch.end()
	}

	fun setSize(width: Int, height: Int) {
		viewport.update(width, height, true)

		batch.transformMatrix = camera.view
		batch.projectionMatrix = camera.projection

		onViewport.set(Vector2i(width, height))
	}

	override fun getBounds(view: TextGraphic, size: Vector2d): Vector2d {
		val gdxFont = view.font as GdxFont
		return gdxFont.calculateBounds(view.text, size, view.wrap, view.symbolWidthMode)
	}

	override fun drawText(view: TextGraphic, position: Vector2d, size: Vector2d) {
		val gdxFont = view.font as GdxFont
		val layout = GlyphLayout(gdxFont.bitmapFont, view.text, 0, view.text.length, view.color.toColor(), size.x.toFloat(), view.horizontalAlign.toHorizontalAlign(), view.wrap, null)
		val x = position.x
		val y = position.y + view.verticalAlign.getPosition(size.y, getLayoutHeight(gdxFont.bitmapFont, layout))
		setColor(view.color.toColor4d())
		gdxFont.bitmapFont.draw(batch, layout, x.toFloat(), y.toFloat())
		if (view.underline) {
			val lineHeight = gdxFont.bitmapFont.data.lineHeight+gdxFont.bitmapFont.data.descent
			layout.runs.forEach { line->
				val lineStart = Vector2d(x, y) + Vector2f(line.x, line.y+ lineHeight).toVector2d()
				drawLine(lineStart, lineStart + Vector2d(line.width.toDouble(), 0.0), view.color.toColor4d(), 2.0)
			}
		}
	}

	private fun getLayoutHeight(font: BitmapFont, layout: GlyphLayout): Double {
		return layout.height.toDouble()
	}

	override fun layout(font: Font, value: String, size: Vector2d, horizontalAlign: Align, verticalAlign: Align, wrap: Boolean): TextLayout {
		val gdxFont = font as GdxFont
		return gdxFont.calculateLayout(value, size, wrap, LayoutAlign(horizontalAlign, verticalAlign))
	}

	fun dispose() {
		batch.dispose()
	}
}
