/**
 * Created by casper on 26.06.2022.
 */
package casper.app;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.util.function.Function;

import casperix.gdx.app.GdxApplicationAdapter;
import casperix.gdx.app.GdxApplicationConfig;
import kotlin.Unit;
import ui.sample.App;

public class MainActivity extends AndroidApplication {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useAccelerometer = false;
		config.useCompass = false;
		config.numSamples = 2;
		initialize(new GdxApplicationAdapter(new GdxApplicationConfig(), watcher -> {
			new App(watcher);
			return null;
		}), config);
	}
}
