package casperix.sample

import casperix.gdx.app.GdxApplicationConfig
import casperix.gdx.app.GdxDesktopApplicationLauncher
import ui.sample.App

fun main(args: Array<String>) {
	GdxDesktopApplicationLauncher(
		GdxApplicationConfig(
			"UI Demo", "icon.png",

		)
	) {
		App(it)
	}
}

