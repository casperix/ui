package casperix.ui.editor

import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import casperix.ui.editor.model.LayoutMode
import casperix.ui.editor.model.NodeModel

class EditorModel {
	val onRoot = StorageSignal(createRoot())

	val cmdChanged = Signal<NodeModel>()
	val cmdSelect = StorageSignal<NodeModel?>(null)
	val cmdAdd = Signal<NodeModel>()
	val cmdRemove = Signal<NodeModel>()
	val cmdSave = EmptySignal()
	val cmdLoad = EmptySignal()

	val actualTarget: NodeModel get() = cmdSelect.value ?: onRoot.value

	fun createRoot(): NodeModel {
		return NodeModel().apply {
			name = "root"
			layout = LayoutMode.VERTICAL
		}
	}

	fun removeSelected() {
		cmdSelect.value?.let { selected ->
			cmdRemove.set(selected)
		}
	}

	fun moveUpSelected() {
		cmdSelect.value?.let { selected ->
			moveUp(selected)
		}
	}

	fun moveDownSelected() {
		cmdSelect.value?.let { selected ->
			moveDown(selected)
		}
	}

	fun getParent(model: NodeModel): NodeModel? {
		val root = onRoot.value
		return searchParent(root, model)
	}

	private fun searchParent(candidate: NodeModel, model: NodeModel): NodeModel? {
		if (candidate.children.contains(model)) return candidate

		candidate.children.forEach { child ->
			searchParent(child, model)?.let { return it }
		}
		return null
	}


	fun moveUp(item: NodeModel):Boolean {
		return moveCustom(item, -1)
	}

	fun moveDown(item: NodeModel):Boolean {
		return moveCustom(item, 1)
	}

	private fun moveCustom(item: NodeModel, delta:Int):Boolean {
		val parent = getParent(item) ?: return false
		if (parent.children.size == 1) return false

		var index = parent.children.indexOf(item)
		if (index + delta < 0 || index + delta >= parent.children.size) return false

		parent.children.remove(item)
		index+= delta
		parent.children.add(index, item)
		cmdChanged.set(parent)
		return true
	}
}

