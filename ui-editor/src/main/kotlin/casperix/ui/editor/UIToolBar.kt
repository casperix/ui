package casperix.ui.editor

import casperix.ui.component.button.ButtonView
import casperix.ui.component.panel.PanelView
import casperix.ui.core.MaxChildrenOrViewDimension
import casperix.ui.core.MinChildrenOrViewDimension
import casperix.ui.core.SizeMode
import casperix.ui.core.UIComponent
import casperix.ui.editor.factory.ElementFactoryStorage
import casperix.ui.layout.Layout

class UIToolBar(val editor: EditorModel) : UIComponent() {
	init {
		PanelView(node)
		node.layout = Layout.HORIZONTAL
		node.placement.sizeMode = SizeMode(MaxChildrenOrViewDimension, MinChildrenOrViewDimension)

		node += ButtonView("remove", UIStyle.buttonSize) {
			editor.removeSelected()
		}
		node += ButtonView("save", UIStyle.buttonSize) { editor.cmdSave.set() }
		node += ButtonView("load", UIStyle.buttonSize) { editor.cmdLoad.set() }

		ElementFactoryStorage.variants.forEach { elementFactory ->
			node += ButtonView("add ${elementFactory.name}", UIStyle.buttonSize) { editor.cmdAdd.set(elementFactory.builder()) }
		}
	}
}