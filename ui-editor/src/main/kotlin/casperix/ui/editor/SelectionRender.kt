package casperix.ui.editor

import casperix.math.color.Color4d
import casperix.math.vector.Vector2d
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.editor.model.NodeModel
import casperix.ui.graphic.ShapeBuilder
import casperix.ui.graphic.ShapeGraphic
import casperix.ui.graphic.ShapeStyle

class SelectionRender(val editor: EditorModel, val target: UINode, val canvas: UINode) : UIComponent(target) {
	private val borderSize = 2.0
	private val selectorStyle = ShapeStyle(Color4d(1.0, 1.0, 1.0, 0.1), borderColor = Color4d(1.0, 0.0, 0.0, 1.0), borderThick = borderSize)
	private var dirty = 2

	init {
		editor.cmdSelect.then { makeDirty() }
		editor.cmdChanged.then { makeDirty() }

		canvas.events.nextFrame.then { updateIfDirty() }
	}

	private fun makeDirty() {
		dirty = 2
	}

	private fun updateIfDirty() {
		if (--dirty == 0) return

		val model = editor.cmdSelect.value
		val tag = if (model != null) {
			NodeModelTagSearch.searchByModel(canvas, model)
		} else null

		target.graphic = if (tag == null || tag.node.parent == null) {
			null
		} else {
			ShapeGraphic(ShapeBuilder.rectangle(tag.node.absolutePosition - canvas.absolutePosition - Vector2d(borderSize / 2.0), tag.node.placement.size + Vector2d(borderSize), selectorStyle))
		}
	}
}