package casperix.ui.editor.serialize

import casperix.ui.core.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.math.roundToInt

object DimensionModeSerializer : KSerializer<DimensionMode> {
	override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("DimensionMode", PrimitiveKind.STRING)

	override fun serialize(encoder: Encoder, value: DimensionMode) {
		val output = when (value) {
			is MinChildrenOrViewDimension -> "min"
			is MaxChildrenOrViewDimension -> "max"
			is ChildrenDimension -> "children"
			is ViewDimension -> "view"
			is ConstDimension -> "const_" + (value.value * 10.0).roundToInt()
			else -> throw Error("Unsupported dimension")
		}
		encoder.encodeString(output)
	}

	override fun deserialize(decoder: Decoder): DimensionMode {
		val input = decoder.decodeString()
		return when (input) {
			"min" -> MinChildrenOrViewDimension
			"max" -> MaxChildrenOrViewDimension
			"children" -> ChildrenDimension
			"view" -> ViewDimension
			else -> {
				val value = input.removePrefix("const_").toIntOrNull() ?: throw Error("Unsupported dimension")
				ConstDimension(value / 10.0)
			}
		}
	}
}