package casperix.ui.editor

import casperix.math.axis_aligned.Box2d
import casperix.math.vector.Vector2d
import casperix.ui.core.UINode
import casperix.ui.editor.model.NodeModel


object NodeModelTagSearch {
	fun searchByModel(node: UINode, model: NodeModel): NodeModelTag? {
		val tag = node.getComponent<NodeModelTag>()
		if (tag != null && tag.model == model) return tag

		node.children.forEach { child ->
			searchByModel(child, model)?.let { childTag ->
				return childTag
			}
		}
		return null
	}

	fun searchByPosition(node: UINode, position: Vector2d): NodeModelTag? {
		node.children.forEach { child ->
			val result = searchByPosition(child, position)
			if (result != null) return result
		}

		val tag = node.getComponent<NodeModelTag>()
		if (tag != null && isInside(node, position)) return tag
		return null
	}

	private fun isInside(node: UINode, position: Vector2d): Boolean {
		return Box2d.byDimension(node.absolutePosition, node.placement.size).isInside(position)
	}
}