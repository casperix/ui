package casperix.ui.editor

import casperix.input.InputDispatcher
import casperix.input.KeyButton
import casperix.signals.switch
import casperix.ui.core.*
import casperix.ui.engine.GdxUIManager

class KeyboardActions(listener: InputDispatcher, val eng: GdxUIManager, val editor: EditorModel) : UIComponent() {
	init {
		listener.onKeyDown.then { key ->
			when (key.button) {
				KeyButton.FORWARD_DELETE -> editor.removeSelected()
				KeyButton.UP -> editor.moveUpSelected()
				KeyButton.DOWN -> editor.moveDownSelected()
				KeyButton.D -> eng.drawer.onDebugSelfArea.switch()
				else -> {

				}
			}
		}
	}
}