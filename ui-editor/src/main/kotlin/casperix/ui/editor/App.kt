package casperix.ui.editor

import casperix.app.window.WindowWatcher
import casperix.ui.component.tab.AbstractTab
import casperix.ui.component.tab.TabMenuView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.ConstDimension
import casperix.ui.core.MaxChildrenOrViewDimension
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.editor.property.editor.UIHierarchyEditor
import casperix.ui.editor.property.editor.UIPropertiesEditor
import casperix.ui.editor.serialize.Serializer
import casperix.ui.engine.GdxUIManager
import casperix.ui.layout.DividerLayout
import casperix.ui.type.LayoutSide
import com.badlogic.gdx.Gdx

class App(watcher: WindowWatcher) {
	init {
		Gdx.gl.glClearColor(0.3f, 0.4f, 0.5f, 1f)

		val engine = GdxUIManager(watcher, true, true)

		val editor = EditorModel()

		val controller = EditorController(editor)

		Serializer(editor)
		val workspace = UIWorkspace(editor)

		val propertyEditor = UIPropertiesEditor(editor)
		val hierarchEditor = UIHierarchyEditor(editor)
		val toolMenu = TabMenuView<AbstractTab>(LayoutSide.TOP)
		toolMenu.tabs += ToggleTab(ToggleView("properties", UIStyle.buttonSize), propertyEditor.node)
		toolMenu.tabs += ToggleTab(ToggleView("hierarchy", UIStyle.buttonSize), hierarchEditor.node)

		KeyboardActions(engine.root.inputs, engine, editor)
		val toolBar = UIToolBar(editor)
		val propertyAndWorkspace = UINode()
		val mainArea = UINode()

		engine.root += mainArea
		DividerLayout(mainArea, toolBar.node, propertyAndWorkspace, LayoutSide.TOP)
		DividerLayout(propertyAndWorkspace, toolMenu.node, workspace.node, LayoutSide.LEFT)
	}
}