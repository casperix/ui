package casperix.ui.editor

import casperix.misc.toPrecision
import casperix.ui.core.*
import casperix.ui.editor.model.*
import casperix.ui.layout.Align
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign

object PropertyFormatter {

	fun componentToName(component: NodeComponent): String {
		return when (component) {
			is PanelComponent -> "Panel"
			is TextComponent -> "TextArea"
			is ButtonComponent -> "Button"
			else -> "<unknown>"
		}
	}

	fun layoutModeToString(layout: LayoutMode): String {
		return when (layout) {
			LayoutMode.NOTHING -> "no"
			LayoutMode.VERTICAL -> "vertical"
			LayoutMode.HORIZONTAL -> "horizontal"
			LayoutMode.SCREEN -> "screen"
			LayoutMode.DIVIDE_LEFT -> "divide-left"
			LayoutMode.DIVIDE_RIGHT -> "divide-right"
			LayoutMode.DIVIDE_TOP -> "divide-top"
			LayoutMode.DIVIDE_BOTTOM -> "divide-bottom"
		}
	}


	fun layoutAlignToString(align: LayoutAlign): String {
		return "(" + horizontalAlignToString(align.horizontal) + "; " + verticalAlignToString(align.vertical) + " )"
	}

	fun alignToString(align: Align): String {
		return when (align) {
			Align.MIN -> "min"
			Align.MAX -> "min"
			Align.CENTER -> "center"
			else -> align.factor.toPrecision(2)
		}
	}

	fun verticalAlignToString(align: Align): String {
		return when (align) {
			Align.MIN -> "top"
			Align.MAX -> "bottom"
			Align.CENTER -> "middle"
			else -> align.factor.toPrecision(2)
		}
	}

	fun horizontalAlignToString(align: Align): String {
		return when (align) {
			Align.MIN -> "left"
			Align.MAX -> "right"
			Align.CENTER -> "middle"
			else -> align.factor.toPrecision(2)
		}
	}

	fun dimensionToString(dimension: DimensionMode): String {
		return when (dimension) {
			is ConstDimension -> "const"
			ViewDimension -> "view"
			ChildrenDimension -> "children"
			MaxChildrenOrViewDimension -> "max"
			MinChildrenOrViewDimension -> "min"
			else -> "unknown"
		}
	}
}