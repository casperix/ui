package casperix.ui.editor.property.custom

import casperix.ui.editor.PropertyFormatter
import casperix.ui.editor.model.*
import casperix.ui.editor.property.EnumProperty
import casperix.ui.editor.property.Property
import casperix.ui.editor.property.ReadonlyProperty
import casperix.ui.editor.property.TextProperty
import casperix.ui.layout.Align

object CommonPropertyBuilder {
	fun setup(selected: NodeModel): List<Property> {
		val result = mutableListOf<Property>()

		result += listOf(
			TextProperty("name", { selected.name ?: "" }, { selected.name = it }),
//			ReadonlyProperty("position", { it.toPrecision(0) }, { selected.placement.position }),
//			ReadonlyProperty("size", { it.toPrecision(0) }, { selected.placement.size }),

			EnumProperty("layout", { PropertyFormatter.layoutModeToString(it) }, LayoutMode.values().toList(), {
				selected.layout
			}, {
				selected.layout = it
			}),

			EnumProperty("align horizontal", { PropertyFormatter.horizontalAlignToString(it) }, listOf(Align.MIN, Align.CENTER, Align.MAX), {
				selected.alignHorizontal
			}, {
				selected.alignHorizontal = it
			}),

			EnumProperty("align vertical", { PropertyFormatter.verticalAlignToString(it) }, listOf(Align.MIN, Align.CENTER, Align.MAX), {
				selected.alignVertical
			}, {
				selected.alignVertical = it
			}),
		)
		result += SizePropertyBuilder.setup(selected)

		selected.component?.let { component ->
			when (component) {
				is TextComponent -> result += TextProperty("text", { component.label }, { component.label = it })
				is ButtonComponent -> {
					result += TextProperty("label", { component.label }, { component.label = it })
				}

				else -> {}
			}
		}

		return result
	}
}