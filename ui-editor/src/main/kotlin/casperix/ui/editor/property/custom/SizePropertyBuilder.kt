package casperix.ui.editor.property.custom

import casperix.ui.core.*
import casperix.ui.editor.PropertyFormatter
import casperix.ui.editor.model.NodeModel
import casperix.ui.editor.property.DoubleProperty
import casperix.ui.editor.property.EnumProperty
import casperix.ui.editor.property.Property

object SizePropertyBuilder {
	private var preferredWidth = 100.0
	private var preferredHeight = 100.0

	fun setup(selected: NodeModel): List<Property> {
		selected.sizeModeWidth.let { dimensionMode ->
			preferredWidth = if (dimensionMode is ConstDimension) dimensionMode.value else 100.0
		}

		selected.sizeModeHeight.let { dimensionMode ->
			preferredHeight = if (dimensionMode is ConstDimension) dimensionMode.value else 100.0
		}

		return listOf(
			EnumProperty("width mode", { PropertyFormatter.dimensionToString(it) }, listOf(ConstDimension(0.0), MinChildrenOrViewDimension, MaxChildrenOrViewDimension, ChildrenDimension, ViewDimension), {
				selected.sizeModeWidth
			}, {
				val actual = if (it is ConstDimension) ConstDimension(preferredWidth) else it
				selected.sizeModeWidth = actual
			}),

			DoubleProperty("width preferred", { preferredWidth }, {
				preferredWidth = it
				if (selected.sizeModeWidth is ConstDimension) {
					selected.sizeModeWidth = ConstDimension(preferredWidth)
				}
			}),

			EnumProperty("height mode", { PropertyFormatter.dimensionToString(it) }, listOf(ConstDimension(0.0), MinChildrenOrViewDimension, MaxChildrenOrViewDimension, ChildrenDimension, ViewDimension), {
				selected.sizeModeHeight
			}, {
				val actual = if (it is ConstDimension) ConstDimension(preferredHeight) else it
				selected.sizeModeHeight = actual
			}),

			DoubleProperty("height preferred", { preferredHeight }, {
				preferredHeight = it
				if (selected.sizeModeHeight is ConstDimension) {
					selected.sizeModeHeight = ConstDimension(preferredHeight)
				}
			}),
		)
	}
}