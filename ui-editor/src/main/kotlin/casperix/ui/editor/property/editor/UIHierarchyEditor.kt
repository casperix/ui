package casperix.ui.editor.property.editor

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.panel.PanelView
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.component.tree.TreeView
import casperix.ui.component.tree.TreeNode
import casperix.ui.core.*
import casperix.ui.editor.EditorModel
import casperix.ui.editor.PropertyFormatter
import casperix.ui.editor.UIStyle
import casperix.ui.editor.model.NodeModel
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign


class UIHierarchyEditor(val editor: EditorModel) : UIComponent(UINode(layout = Layout.VERTICAL)) {
	private val propertyNameSize = Vector2d(150.0, 30.0)

	private val headerText = TextView("")
	private val tree = TreeView()

	init {
		PanelView(node)
		node.placement.sizeMode = SizeMode(UIStyle.basicPanelWidth, MaxChildrenOrViewDimension)
		node += UINode().apply {
			this += headerText
		}
		node += tree

		editor.onRoot.then { rebuild() }
		editor.cmdAdd.then { rebuild() }
		editor.cmdRemove.then { rebuild() }
		editor.cmdChanged.then { rebuild() }

		node.events.propertyChanged.then {
			node.root.getSkin<ToggleSkin>("selectable")?.let {
				node.setSkin(it)
			}
		}
	}

	private fun rebuild() {
		val root = editor.onRoot.value
		tree.setRoot(false, buildTree(root))
	}

	private fun buildTree(model: NodeModel): TreeNode {
		val node = TreeNode(buildLeaf(model), true)

		node.children += model.children.map { buildTree(it) }
		return node
	}

	private fun buildLeaf(model: NodeModel): UINode {
		val infos = mutableListOf<String>()

		val name = model.name
		if (name != null) infos += "\"$name\"" else infos += "<unnamed>"

		model.component?.let { component ->
			infos += PropertyFormatter.componentToName(component)
		}

		val buttonSignal = StorageSignal(editor.cmdSelect.value == model)
		buttonSignal.then {
			if (it) {
				if (editor.cmdSelect.value != model) {
					editor.cmdSelect.value = model
				}
			} else {
				if (editor.cmdSelect.value == model) {
					editor.cmdSelect.value = null
				}
			}
		}

		editor.cmdSelect.then {
			buttonSignal.value = (it == model)
		}


		val toggle = ToggleView("", UIStyle.leafSize, buttonSignal)
		toggle.button.content.layout = Layout.SCREEN
		toggle.button.content.children.clear()
		toggle.button.content.placement.sizeMode = SizeMode.max
		toggle.button.content += TextView(infos.joinToString(" - ")).apply {
			this.node.placement.sizeMode = SizeMode.min
			this.node.placement.align = LayoutAlign.LEFT
		}
		return toggle.node
	}


}


