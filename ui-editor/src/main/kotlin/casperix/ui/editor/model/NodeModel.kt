package casperix.ui.editor.model

import casperix.math.vector.Vector2d
import casperix.ui.core.DimensionMode
import casperix.ui.core.MaxChildrenOrViewDimension
import casperix.ui.editor.serialize.AlignSerializer
import casperix.ui.editor.serialize.DimensionModeSerializer
import casperix.ui.layout.Align
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class NodeModel() {
	var name: String? = null
	var layout = LayoutMode.SCREEN
	val children = mutableListOf<NodeModel>()
	var component: NodeComponent? = null

	@Serializable(with = DimensionModeSerializer::class)
	var sizeModeWidth: DimensionMode = MaxChildrenOrViewDimension

	@Serializable(with = DimensionModeSerializer::class)
	var sizeModeHeight: DimensionMode = MaxChildrenOrViewDimension

	@Serializable(with = AlignSerializer::class)
	var alignHorizontal = Align.CENTER

	@Serializable(with = AlignSerializer::class)
	var alignVertical = Align.CENTER
}

enum class LayoutMode {
	NOTHING,
	SCREEN,
	HORIZONTAL,
	VERTICAL,
	DIVIDE_LEFT,
	DIVIDE_RIGHT,
	DIVIDE_TOP,
	DIVIDE_BOTTOM,
}

sealed interface NodeComponent

@Serializable
@SerialName("PanelComponent")
object PanelComponent : NodeComponent

@Serializable
@SerialName("ButtonComponent")
class ButtonComponent(var label: String) : NodeComponent

@Serializable
@SerialName("TextComponent")
class TextComponent(var label: String) : NodeComponent
