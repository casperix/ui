package casperix.ui.editor

import casperix.math.vector.Vector2d
import casperix.ui.core.ConstDimension

object UIStyle {
	val basicPanelWidth = ConstDimension(320.0)

	val buttonSize = Vector2d(90.0, 30.0)

	val propertyLabelSize = Vector2d(140.0, 40.0)
	val cellSize = Vector2d(170.0, 40.0)
	val leafSize = Vector2d(180.0, 40.0)
}