package casperix.ui.component.timer

import casperix.ui.core.UINode
import kotlin.random.Random

class TestTimerScene {
	val root = UINode()
	var tick = 0.0
	var time = 0.0
	var frame = 0

	companion object {
		val random = Random(1)
		val MAX_TICK = 5.0
		val times = (1..1000).map { random.nextDouble() * MAX_TICK }

		fun run(onStartScene: (TestTimerScene) -> Unit, onFinishScene: (TestTimerScene) -> Unit) {
			val scene = TestTimerScene()

			onStartScene(scene)

			times.forEach { tick ->
				scene.tick = tick
				updateFrame(scene.root, tick)
				scene.frame++
				scene.time += tick
			}

			onFinishScene(scene)
		}

		private fun updateFrame(node: UINode, tick:Double) {
			node.events.nextFrame.set(tick)
			node.children.forEach {
				updateFrame(it, tick)
			}

		}
	}
}