package casperix.ui.font

data class FontWeight(val value:Int) {
		companion object {
			val NORMAL = FontWeight(400)
			val BOLD = FontWeight(700)
		}
}