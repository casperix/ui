package casperix.ui.font

enum class SymbolWidthMode {
	/**	Default width	 */
	DEFAULT,

	/** All digits have equal width	*/
	DIGITS,

	/** All symbols have equal width	*/
	MONOSPACE,
}