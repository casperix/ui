package casperix.ui.font

import casperix.math.vector.Vector2d
import casperix.ui.engine.TextLayout
import casperix.ui.graphic.TextGraphic
import casperix.ui.source.FontSource
import casperix.ui.type.LayoutAlign

interface Font {
	val source: FontSource
	val name:String
	val size:Float
	val kerning:Boolean
	val italic:Boolean
	val weight: FontWeight
	val metrics: FontMetrics

	/**
	 * monospace -- all char have equal width
	 */
	fun calculateBounds(text: String, space: Vector2d, wrap: Boolean, symbolWidthMode:SymbolWidthMode): Vector2d

	fun calculateLayout(text: String, space: Vector2d, wrap: Boolean, align: LayoutAlign): TextLayout

	fun calculateBounds(graphic: TextGraphic): Vector2d {
		return calculateBounds(graphic.text, Vector2d(Double.MAX_VALUE), graphic.wrap, graphic.symbolWidthMode)
	}

	fun calculateLayout(graphic: TextGraphic): TextLayout{
		return calculateLayout(graphic.text, Vector2d(Double.MAX_VALUE), graphic.wrap, LayoutAlign(graphic.horizontalAlign, graphic.verticalAlign))
	}

}