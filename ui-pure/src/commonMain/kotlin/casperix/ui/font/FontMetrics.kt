package casperix.ui.font

data class FontMetrics(
	val lineHeight: Double,
	val capHeight: Double,
	val xHeight: Double,
	val ascent: Double,
	val descent: Double,
)