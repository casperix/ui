package casperix.ui.engine

import casperix.signals.concrete.StorageFuture
import casperix.math.color.Color4d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.math.axis_aligned.Box2d
import casperix.math.axis_aligned.Box2i
import casperix.math.geometry.*
import casperix.math.vector.rotateCCW
import casperix.math.vector.toQuad
import casperix.ui.font.Font
import casperix.ui.graphic.TextGraphic
import casperix.ui.layout.Align
import casperix.ui.type.LayoutAlign
import kotlin.math.PI

interface Drawer {
	val onViewport: StorageFuture<Vector2i>

	fun setScissor(area: Box2i?)
	fun getScissor(): Box2i?

	fun drawText(view: TextGraphic, position: Vector2d, size: Vector2d)

	@Deprecated(message = "Use Font:calculateBounds")
	fun getBounds(view: TextGraphic, size: Vector2d): Vector2d {
		return view.font.calculateBounds(view.text, size, view.wrap, view.symbolWidthMode)
	}

	@Deprecated(message = "Use Font:calculateLayout")
	fun layout(font: Font, value: String, size: Vector2d, horizontalAlign: Align, verticalAlign: Align, wrap: Boolean): TextLayout {
		return font.calculateLayout(value, size, wrap, LayoutAlign(horizontalAlign, verticalAlign))
	}

	fun drawTransformedBitmap(bitmap: Bitmap, targetArea: Quad2d, color: Color4d = Color4d.ONE)

	fun drawBitmap(bitmap: Bitmap, targetPosition: Vector2d, targetSize: Vector2d, sourcePosition: Vector2i = bitmap.region.min, sourceSize: Vector2i = bitmap.region.dimension, color: Color4d = Color4d.ONE)

	fun drawLine(line: Line2d, color: Color4d, thick: Double) {
		drawLine(line.v0, line.v1, color, thick)
	}

	fun drawLine(A: Vector2d, B: Vector2d, color: Color4d, thick: Double) {
		val direction = (B - A).normalize()
		val right = direction.rotateCCW() * thick / 2.0
		val quad = Quad2d(A - right, A + right, B + right, B - right)

		drawQuad(quad, color)
	}

	fun drawTransformedBitmap(bitmap: Bitmap, shapeCenter: Vector2d, shapeSize: Vector2d, color: Color4d = Color4d.ONE, flipX: Boolean = false, flipY: Boolean = false, rotateDegreeCW: Double = 0.0) {
		if (!flipX && !flipY && rotateDegreeCW == 0.0) {
			drawBitmap(bitmap, shapeCenter - shapeSize / 2.0, shapeSize, color = color)
		} else {
			var transformedArea = Box2d.byDimension(-shapeSize / 2.0, shapeSize).toQuad().rotate(rotateDegreeCW * PI / 180.0)
			transformedArea = transformedArea.translate(shapeCenter)
			if (flipX) transformedArea = flipX(transformedArea)
			if (flipY) transformedArea = flipY(transformedArea)

			drawTransformedBitmap(bitmap, transformedArea, color)
		}
	}

	fun drawTriangle(targetArea: Triangle2d, color: Color4d)

	fun drawQuad(targetArea: Quad2d, color: Color4d) {
		drawTriangle(targetArea.getFace(0), color)
		drawTriangle(targetArea.getFace(1), color)
	}

	private fun flipX(source: Quad2d): Quad2d {
		return Quad2d(source.v1, source.v0, source.v3, source.v2)
	}

	private fun flipY(source: Quad2d): Quad2d {
		return Quad2d(source.v3, source.v2, source.v1, source.v0)
	}

}
