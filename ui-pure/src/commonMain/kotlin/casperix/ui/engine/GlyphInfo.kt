package casperix.ui.engine

import casperix.math.vector.Vector2d

class GlyphInfo(
	val symbolIndex:Int,
	val position:Vector2d,	//	Позиция ячейки символа
	val offset:Vector2d,	//	Смещение изображения внутри ячейки
	val size:Vector2d,	//	Размер символа
)