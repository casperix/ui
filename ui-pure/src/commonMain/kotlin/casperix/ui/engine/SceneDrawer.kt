package casperix.ui.engine

import casperix.misc.collapse
import casperix.math.intersection.hasIntersectionBoxWithBox
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.math.axis_aligned.Box2d
import casperix.math.axis_aligned.Box2i
import casperix.math.color.Color
import casperix.math.vector.toQuad
import casperix.ui.component.DebugSkin
import casperix.ui.core.UINode
import casperix.ui.graphic.TextGraphic
import casperix.ui.layout.Align

data class DebugMode(val skin: DebugSkin, val showSelf: Boolean, val showSelfBorder: Boolean, val showSelfGap: Boolean, val showChildren: Boolean,  val showName: Boolean)

object SceneDrawer {
	var filterOverScreenElements: Boolean = true

	fun draw(drawer: Drawer, root: UINode, debugMode: DebugMode) {
		invalidateLayout(root, true)
		invalidateLayout(root, false)
		draw(drawer, root, Vector2d.ZERO, null, null)
		if (debugMode.showName || debugMode.showChildren || debugMode.showSelf || debugMode.showSelfBorder || debugMode.showSelfGap) {
			draw(drawer, root, Vector2d.ZERO, null, debugMode)
		}
	}


	/**
	 * @param debugSkin -- give debug skin for show debug information
	 */
	private fun draw(drawer: Drawer, node: UINode, parentAbsolutePosition: Vector2d, parentMask: Box2i?, debugMode: DebugMode?) {

		if (node.clippingContent && debugMode == null) {
			val nodePosition = node.placement.position + parentAbsolutePosition
			val nodeSize = node.placement.size
			val mask = getClippingMask(nodePosition, parentMask, nodeSize) ?: return

			drawer.setScissor(mask)
			drawNodes(drawer, node, parentAbsolutePosition, mask, debugMode)
			drawer.setScissor(parentMask)
		} else {
			drawNodes(drawer, node, parentAbsolutePosition, parentMask, debugMode)
		}
	}

	private fun drawNodes(drawer: Drawer, node: UINode, parentAbsolutePosition: Vector2d, mask: Box2i?, debugMode: DebugMode?) {
		val absolutePosition = node.placement.position + parentAbsolutePosition

		if (debugMode == null) node.graphic?.draw(drawer, node.placement.position + parentAbsolutePosition, node.placement.size)
		if (node.children.isNotEmpty()) drawChildren(drawer, node, absolutePosition, mask, debugMode)

		if (debugMode != null) {
			val placement = node.placement
			if (placement.size.greater(Vector2d.ZERO)) {
				if (debugMode.showSelf) {
					debugMode.skin.selfArea.draw(drawer, placement.position + parentAbsolutePosition, placement.size)
				}
				if (debugMode.showSelfBorder ) {
					debugMode.skin.borderArea.draw(drawer, placement.position + parentAbsolutePosition - placement.border.leftTop, placement.size + placement.border.size)
				}
				if (debugMode.showSelfBorder) {
					debugMode.skin.borderArea.draw(drawer, placement.position + parentAbsolutePosition - placement.border.leftTop - placement.gap.leftTop, placement.size + placement.border.size + placement.gap.size)
				}
			}

			if (debugMode.showChildren && node.layoutExecutor.childrenSize.greater(Vector2d.ZERO)) {
				val childrenPosition = node.children.collapse(Vector2d.ZERO) { child, position -> position.lower(child.absolutePosition)}
				val childrenSize = node.layoutExecutor.childrenSize
				debugMode.skin.childrenArea.draw(drawer, childrenPosition, childrenSize)
			}

			if (debugMode.showName) {
				val graphic = TextGraphic(node.name ?: "?", debugMode.skin.font, Color.WHITE, false, Align.MIN, Align.MIN)
				val size = graphic.font.calculateBounds(graphic)
				val border = Vector2d(2.0, 1.0)

				val area = Box2d.byDimension(node.placement.position + parentAbsolutePosition - border, size + border * 2.0)

				drawer.drawQuad(Box2d(area.min - Vector2d(1.0), area.max + Vector2d(1.0)).toQuad(), Color.WHITE.toColor4d())
				drawer.drawQuad(area.toQuad(), Color.BLACK.toColor4d())
				drawer.drawText(graphic, node.placement.position + parentAbsolutePosition, Vector2d(100.0))
			}
		}
	}

	private fun drawChildren(drawer: Drawer, node: UINode, absolutePosition: Vector2d, mask: Box2i?, debugMode: DebugMode?) {
		val viewport = if (!filterOverScreenElements) null else {
			mask?.toBox2d() ?: Box2i.byDimension(Vector2i.ZERO, drawer.onViewport.value).toBox2d()
		}
		node.childrenOrdered().forEach {
			if (viewport == null || it.graphic == null || hasIntersectionBoxWithBox(viewport, Box2d.byDimension(it.absolutePosition, it.placement.size.upper(it.placement.getBound().clientSize)))) {
				draw(drawer, it, absolutePosition, mask, debugMode)
			}
		}
	}

	private fun getClippingMask(absolutePosition2d: Vector2d, parentMask: Box2i?, nodeSize:Vector2d): Box2i? {
		val absolutePosition = absolutePosition2d.roundToVector2i()

		val absoluteSize = nodeSize.roundToVector2i()
		if (!absoluteSize.greater(Vector2i.ZERO)) return null

		val mask = Box2i.byDimension(absolutePosition, absoluteSize)
		if (parentMask == null) return mask

		val min = parentMask.min.upper(mask.min)
		val max = parentMask.max.lower(mask.max)
		if (!max.greater(min)) return null

		return Box2i(min, max)
	}

	private fun invalidateLayout(node: UINode, bubbling: Boolean) {
		if (!bubbling) {
			node.layoutExecutor.invalidateLayout()
		}
		node.childrenOrdered().forEach { invalidateLayout(it, bubbling) }
		if (bubbling) {
			node.layoutExecutor.invalidateLayout()
		}
	}

}