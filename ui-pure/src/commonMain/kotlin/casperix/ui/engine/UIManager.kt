package casperix.ui.engine

import casperix.ui.core.UINode


interface UIManager  {
	val root:UINode
	val drawer: Drawer
	val clipboard: Clipboard
	val supplier:Supplier
}