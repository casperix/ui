package casperix.ui.engine

interface Clipboard {
	fun getContent(): String?
	fun setContent(value: String)
}