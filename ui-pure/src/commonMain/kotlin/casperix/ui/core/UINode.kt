package casperix.ui.core

import casperix.input.PointerEvent
import casperix.signals.collection.observableSetOf
import casperix.signals.collection.observableUniqueListOf
import casperix.misc.disposeAll
import casperix.math.vector.Vector2d
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.graphic.Graphic
import casperix.ui.input.NodeEventDispatcher
import casperix.ui.input.NodeInputDispatcher
import casperix.ui.input.TouchCatcher
import casperix.ui.input.TouchCatcherFactory
import casperix.ui.layout.Align
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign

class UINode private constructor(sizeMode: SizeMode, align: LayoutAlign, layout: Layout?, name: String?, graphic: Graphic?, children: List<UINode>?) {

	companion object {
		class InvalidHierarchy : Error()
		class InvalidParentSize(val value: Vector2d) : Error()
		class InvalidParentPosition(val value: Vector2d) : Error()
	}


	/**
	 * 	Создать элемент определенного размера, в определенном выравнивании.
	 * 	Хорош для кнопок, иконок, подписей --  когда известен необходимый размер
	 */
	constructor(
		constSize: Vector2d, horizontalAlign: Align = Align.CENTER, verticalAlign: Align = Align.CENTER,
		layout: Layout? = Layout.SCREEN, name: String? = null, graphic: Graphic? = null, children: List<UINode>? = null
	)
			: this(SizeMode.const(constSize), LayoutAlign(horizontalAlign, verticalAlign), layout, name, graphic, children)

	/**
	 * 	По умолчанию элемент растягивается на всю доступную область
	 * 	Хорош для элементов иерархии, окон, панелей -- когда предполагается занять всю предоставленную родителем область
	 */
	constructor(
		align: LayoutAlign = LayoutAlign.CENTER_CENTER,
		layout: Layout? = Layout.SCREEN, name: String? = null, graphic: Graphic? = null, children: List<UINode>? = null, sizeBehaviour: SizeMode = SizeMode.max
	)
			: this(sizeBehaviour, align, layout, name, graphic, children)

	/**
	 * 	Имя элемента
	 * 	Исключительно для удобства различения компонентов
	 * 	По умолчанию автоматически заполняется уникальным именем
	 */
	var name: String? = null

	val inputs get() = getOrCreateInput()

	val events = NodeEventDispatcher()

	/**
	 * 	Правила расположения дочерних элементов.
	 * 	Layout задает каждому ребенку определенную область
	 */
	var layout: Layout? by events.onLayout.observable(layout)

	/**
	 *		Графический компонент. Рендер текущей ноды
	 */
	var graphic: Graphic? by events.onGraphic.observable(null)

	/**
	 * 	Список детей.
	 * 	@throws  InvalidHierarchy При добавлении родителя (или элемента еще выше по иерархии)
	 *
	 */
	val children = observableUniqueListOf<UINode>()

	/**
	 * 	Список компонентов.
	 * 	Компонент навсегда привязан к ноде
	 * 	Компонент добавляется и удаляется из списка автоматически:
	 */
	val components: Collection<UIComponent> get() = componentsSource

	/**
	 * 	Прямой порядок отрисовки рисует дочерние элементы от первого до последнего (последний сверху)
	 * 	Обратный -- наоборот (первый сверху)
	 */
	var directDrawChildren = true

	/**
	 * 	По умолчанию область элемента НЕ влияет на доступную область рисования
	 * 	Включенный режим обрезает все пикселы заданного элемента (и всех его детей) по области этого элемента
	 */
	var clippingContent = false

	/**
	 * 	Коллекция свойств.
	 * 	Отсутствующий элемент ищется в родительской коллекции свойств
	 */
	val properties = HierarchicalStorage(this)

	/**
	 * 	Возможность данного элемента "ловить" событие.
	 * 	@see TouchCatcher
	 */
	var touchFilterBuilder: (UINode, PointerEvent) -> TouchCatcher = TouchCatcherFactory::inside

	/**
	 * 	Корень иерархии
	 */
	val root: UINode get() = parent?.root ?: this

	var parent: UINode? = null; private set


	/**
	 * 	Глобальное положение элемента.
	 * 	(В системе отсчета root)
	 */
	val absolutePosition: Vector2d get() = (parent?.absolutePosition ?: Vector2d.ZERO) + placement.position

	val placement = Placement(this)


	internal val layoutExecutor = LayoutCalculator(this)
	internal val componentsSource = observableSetOf<UIComponent>()
	internal var inputsReceiver: NodeInputDispatcher? = null

	init {
		placement.sizeMode = sizeMode
		placement.align = align

		if (name != null) {
			this.name = name
		}
		if (graphic != null) {
			this.graphic = graphic
		}


		this.children.then({ nextChild ->
			if (nextChild.parent != this) {
				setParent(this, nextChild)
			}
		}, { lastChild ->
			if (lastChild.parent == this) {
				setParent(null, lastChild)
			}
		})

		children?.let {
			this.children += it
		}
		parent?.let {
			it += this
		}
	}


	private fun getOrCreateInput(): NodeInputDispatcher {
		var source = inputsReceiver
		if (source == null) {
			source = NodeInputDispatcher()
			inputsReceiver = source
		}
		return source
	}

	private fun setParent(parent: UINode?, child: UINode) {
		if (checkContainsHierarchy(child)) throw InvalidHierarchy()
		val equalsParent = child.parent == parent
		if (!equalsParent) child.parent?.events?.onRemoveChild?.set(child)
		child.parent = parent
		child.properties.dispatchChanged()
		child.events.onParent.set(parent)
		if (!equalsParent) {
			child.setRoot()
			child.parent?.events?.onAddChild?.set(child)
		}
	}

	private fun setRoot() {
		events.onRoot.set(root)
		children.forEach {
			it.setRoot()
		}
	}

	private fun checkContainsHierarchy(value: UINode): Boolean {
		if (this == value) return true
		return parent?.checkContainsHierarchy(value) ?: false
	}


	inline fun <reified T> getComponent(): T? {
		components.forEach {
			if (it is T) return it
		}
		return null
	}

	fun childrenOrdered(invert: Boolean = false): List<UINode> {
		return if (directDrawChildren == !invert) {
			children
		} else {
			children.asReversed()
		}
	}

	override fun toString(): String {
		val parent = parent
		val name = name
		return (if (parent != null) "$parent." else "") + if (name != null && name.length > 0) name else "?"
	}

	fun removeSelf():Boolean {
		val parent = parent ?: return false
		parent.removeChildren(this)
		return true
	}

	fun addChildren(vararg other: UINode) {
		children.addAll(other)
	}

	fun addChildren(vararg other: UIComponent) {
		children.addAll(other.map { it.node })
	}

	fun removeChildren(vararg other: UINode) {
		children.removeAll(other.toSet())
	}

	fun removeChildren(vararg other: UIComponent) {
		children.removeAll(other.map { it.node })
	}

	operator fun plusAssign(other: UINode) {
		addChildren(other)
	}

	operator fun minusAssign(other: UINode) {
		removeChildren(other)
	}

	operator fun plusAssign(other: UIComponent) {
		addChildren(other)
	}

	operator fun minusAssign(other: UIComponent) {
		removeChildren(other)
	}

	fun moveToFront() {
		val parent = parent ?: return
		if (this != parent.children.last()) {
			parent.children.add(parent.children.size - 1, this)
		}
	}

	fun moveToBack() {
		val parent = parent ?: return
		if (this != parent.children.first()) {
			parent.children.add(0, this)
		}
	}

	/**
	 * 	Set one and only one child
	 * 	clear all last children & components
	 */
	fun singleton(value: UINode) {
		children.clear()
		componentsSource.disposeAll()
		children += value
	}

	fun singleton(value: UIComponent) {
		singleton(value.node)
	}

	inline fun <reified SkinClass : Any> setSkin(value:SkinClass, name: String? = null) {
		properties.set(value::class, value, name)
	}

	inline fun <reified SkinClass : Any> getSkin(name: String? = null):SkinClass? {
		return properties.get(SkinClass::class, name)
	}

	inline fun <reified SkinClass : Any> setSkinByName(sourceName:String?, targetName: String? = null):Boolean {
		val skin = getSkin<SkinClass>(sourceName) ?: return false
		setSkin(skin, targetName)
		return true
	}

}