package casperix.ui.core

import casperix.math.vector.Vector2d
import casperix.misc.toPrecision

sealed interface DimensionMode

class ConstDimension(val value: Double) : DimensionMode {
	init {
		if (!value.isFinite() || value < 0.0) throw Error("Expected finite, non negative value, but actual is $value")
	}

	override fun toString(): String {
		return "(${value.toPrecision(1)})"
	}


}

object ChildrenDimension : DimensionMode
object ViewDimension : DimensionMode
object MinChildrenOrViewDimension : DimensionMode
object MaxChildrenOrViewDimension : DimensionMode

data class SizeMode(val width: DimensionMode, val height: DimensionMode) {
	companion object {
		fun const(value: Vector2d): SizeMode {
			return SizeMode(ConstDimension(value.x), ConstDimension(value.y))
		}

		fun constWidth(width:Double):SizeMode {
			return SizeMode(ConstDimension(width), MaxChildrenOrViewDimension)
		}

		fun constHeight(height:Double):SizeMode {
			return SizeMode(MaxChildrenOrViewDimension, ConstDimension(height))
		}

		val view = SizeMode(ViewDimension, ViewDimension)
		val children = SizeMode(ChildrenDimension, ChildrenDimension)
		val min = SizeMode(MinChildrenOrViewDimension, MinChildrenOrViewDimension)
		val max = SizeMode(MaxChildrenOrViewDimension, MaxChildrenOrViewDimension)
	}

	override fun toString(): String {
		return "" + dimToString(width) + "X" + dimToString(height)
	}

	private fun dimToString(mode: DimensionMode): String {
		return when (mode) {
			is ConstDimension -> mode.value.toPrecision(1)
			is ViewDimension -> "view"
			is ChildrenDimension -> "child"
			is MinChildrenOrViewDimension -> "min"
			is MaxChildrenOrViewDimension -> "max"
		}
	}
}
