package casperix.ui.core

import casperix.misc.Disposable
import casperix.misc.DisposableHolder
import casperix.ui.engine.UIManager

open class UIComponent(val node: UINode = UINode()) : DisposableHolder() {
	val engine get() = node.properties.get(UIManager::class)

	init {
		node.componentsSource += this
	}

	override fun dispose() {
		super.dispose()
		node.componentsSource -= this
	}

	companion object {
		fun wrap(node: UINode, item: Disposable): UIComponent {
			return object : UIComponent(node) {
				init {
					components.add(item)
				}
			}
		}
	}
}