package casperix.ui.core

import casperix.misc.toPrecision
import casperix.math.vector.Vector2d
import casperix.misc.max
import casperix.ui.type.LayoutSide

data class SideIndents(val left: Double, val top: Double, val right: Double, val bottom: Double) {
	constructor(space: Double) : this(space, space, space, space)
	constructor(spaceX: Double, spaceY: Double) : this(spaceX, spaceY, spaceX, spaceY)
	constructor(left: Int, top: Int, right: Int, bottom: Int) : this(left.toDouble(), top.toDouble(), right.toDouble(), bottom.toDouble())
	constructor(space: Int) : this(space, space, space, space)
	constructor(spaceX: Int, spaceY: Int) : this(spaceX, spaceY, spaceX, spaceY)

	override fun toString(): String {
		return "SideIndents(left=${left.toPrecision(1)}, top=${top.toPrecision(1)}, right=${right.toPrecision(1)}, bottom=${bottom.toPrecision(1)})"
	}

	val leftTop: Vector2d get() = Vector2d(left, top)
	val rightBottom: Vector2d get() = Vector2d(right, bottom)
	val size: Vector2d get() = Vector2d(left + right, top + bottom)

	operator fun plus(other: SideIndents):SideIndents {
		return SideIndents(left + other.left, top + other.top, right + other.right, bottom + other.bottom)
	}

	fun gapFromSide(side: LayoutSide): Double {
		return when (side) {
			LayoutSide.LEFT -> left
			LayoutSide.RIGHT -> right
			LayoutSide.TOP -> top
			LayoutSide.BOTTOM -> bottom
		}
	}

	fun upper(other:SideIndents):SideIndents {
		return SideIndents(
			max(this.left, other.left),
			max(this.top, other.top),
			max(this.right, other.right),
			max(this.bottom, other.bottom),
		)
	}

	companion object {
		val ZERO = SideIndents(0)
	}
}