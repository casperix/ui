package casperix.ui.core

import casperix.math.vector.Vector2d

class LayoutCalculator(val node: UINode) {

	internal fun invalidateRequest() {
		dirtyLayout = true
	}

	internal fun invalidateParentRequest() {
		node.parent?.layoutExecutor?.invalidateRequest()
	}

	/**
	 * 	Размер содержимого элемента.
	 * 	Определяется автоматически, через "layout"
	 */
	var childrenSize:Vector2d by node.events.onChildrenSize.observable(Vector2d.ZERO); private set


	private val events = node.events
	private var dirtyLayout = false

	init {
		events.onSizeMode.then { invalidateRequest() }
		events.onChildrenSize.then { invalidateRequest() }
		events.onSize.then { invalidateRequest() }
		events.onParent.then { invalidateRequest() }
		events.onLayout.then { invalidateRequest() }

		events.onSizeMode.then { invalidateParentRequest() }
		events.onChildrenSize.then { invalidateParentRequest() }
		events.onBorder.then { invalidateParentRequest() }
		events.onSize.then { invalidateParentRequest() }
		events.onAlign.then { invalidateParentRequest() }

		node.children.then({
			invalidateRequest()
		}, { _ ->
			invalidateRequest()
		})
	}

	internal fun invalidateLayout() {
		if (!dirtyLayout) return
		dirtyLayout = false
		childrenSize = node.layout?.update(node.children.map { it.placement }, node.placement.size) ?: Vector2d.ZERO
	}

}