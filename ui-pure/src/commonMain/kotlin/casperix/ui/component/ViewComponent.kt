package casperix.ui.component

import casperix.signals.then
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import kotlin.reflect.KClass

abstract class ViewComponent<Skin : Any>(val clazz: KClass<Skin>, node: UINode) : UIComponent(node) {
	private var usedSkin: Skin? = null

	var skin: Skin?
		get() {
			return usedSkin
		}
		set(value) {
			if (usedSkin == value) return

			if (value != null) {
				node.properties.set(clazz, value)
			} else {
				node.properties.unset(clazz)
			}
		}

	init {
		node.events.propertyChanged.then(components) {
			updateSkin()
		}
	}

	private fun updateSkin() {
		val nextSkin = node.properties.get(clazz)

		if (usedSkin == nextSkin) return
		usedSkin = nextSkin

		if (nextSkin != null) {
			applySkin(nextSkin)
		}
	}

	protected abstract fun applySkin(skin: Skin)
}