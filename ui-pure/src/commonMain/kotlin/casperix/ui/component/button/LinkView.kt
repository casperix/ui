package casperix.ui.component.button

import casperix.signals.then
import casperix.ui.component.text.TextView
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class LinkView(label: String, val logic: ButtonLogic, node:UINode) : UIComponent(node) {
	constructor(label: String, logic: ButtonLogic) : this(label, logic, logic.node)

	constructor(label: String, onClick: () -> Unit) : this(label, ButtonLogic(UINode(), onClick))

	val skin: LinkViewSkin? get() = node.properties.get(LinkViewSkin::class)
	val text = TextView(label, true, false, node)

	init {
		logic.onChanged.then(components) { refresh() }
		node.events.propertyChanged.then(components) { refresh() }
	}

	private fun refresh() {
		val skin = skin ?: return

		val textSkin = if (!logic.enabled) {
			skin.disabled
		} else if (logic.pressed) {
			skin.pressed
		} else if (logic.focused) {
			skin.focused
		} else {
			skin.normal
		}

		node.properties.set(textSkin)
	}
}