package casperix.ui.component.button

import casperix.ui.component.text.TextSkin

data class LinkViewSkin(
	val disabled: TextSkin,
	val normal: TextSkin,
	val focused: TextSkin,
	val pressed: TextSkin,
)