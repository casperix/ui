package casperix.ui.component.button

import casperix.input.PointerEvent
import casperix.math.vector.Vector2d
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.component.bitmap.BitmapView
import casperix.ui.component.text.TextView
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.engine.Bitmap
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.ScalingUniform
import casperix.ui.input.TouchCatcher
import casperix.ui.input.TouchCatcherFactory
import casperix.ui.layout.Layout

class ButtonView(val logic: ButtonLogic, node: UINode) : ViewComponent<ButtonSkin>(ButtonSkin::class, node) {
	constructor(logic: ButtonLogic) : this(logic, logic.node)

	constructor(label: String, preferredSize: Vector2d, onClick: () -> Unit) : this(ButtonLogic(UINode(constSize = preferredSize), onClick)) {
		addLabel(label)
	}

	constructor(bitmap: Bitmap, preferredSize: Vector2d, onClick: () -> Unit) : this(ButtonLogic(UINode(constSize = preferredSize), onClick)) {
		addBitmap(bitmap)
	}

	constructor(label: String, bitmap: Bitmap, preferredSize: Vector2d, onClick: () -> Unit) : this(ButtonLogic(UINode(constSize = preferredSize), onClick)) {
		addLabel(label)
		addBitmap(bitmap)
	}

	constructor(label: String, graphic: BitmapGraphic, preferredSize: Vector2d, onClick: () -> Unit) : this(ButtonLogic(UINode(constSize = preferredSize), onClick)) {
		addLabel(label)
		addBitmapGraphic(graphic)
	}

	constructor(graphic: BitmapGraphic, label: String, preferredSize: Vector2d, onClick: () -> Unit) : this(ButtonLogic(UINode(constSize = preferredSize), onClick)) {
		addBitmapGraphic(graphic)
		addLabel(label)
	}

	constructor(bitmap: Bitmap, label: String, preferredSize: Vector2d, onClick: () -> Unit) : this(ButtonLogic(UINode(constSize = preferredSize), onClick)) {
		addBitmap(bitmap)
		addLabel(label)
	}

	val content = UINode(layout = Layout.VERTICAL)
	private val graphic = ButtonGraphic(logic, skin)

	init {
		if (node.name == null) node.name = "button"
		node.graphic = graphic
		node.touchFilterBuilder = ::skinBasedTouchFilter
		content.placement.sizeMode = SizeMode.children
		node += content

		logic.onChanged.then(components) {
			refreshTextSkin()
		}
	}

	override fun dispose() {
		super.dispose()
		graphic.dispose()
	}

	private fun skinBasedTouchFilter(node: UINode, event: PointerEvent): TouchCatcher {
		val default = TouchCatcherFactory.inside(node, event)
		val skin = skin ?: return default

		val position = node.absolutePosition
		val size = node.placement.size
		val border = skin.hitBorder

		if (event.position.greaterOrEq(position + border.leftTop) && event.position.lessOrEq(position + size - border.rightBottom)) {
			return TouchCatcher(true, true)
		}

		return TouchCatcher(false, false)
	}

	fun clearContent() {
		content.children.clear()
	}

	fun addLabel(value: String) {
		content += TextView(value).node
	}

	fun setContentLayout(layout: Layout) {
		content.layout = layout
	}

	fun addBitmap(bitmap: Bitmap, scale: Double = 1.0) {
		content += BitmapView(BitmapGraphic(bitmap, ScalingUniform(scale), SideIndents(5.0))).node
	}

	fun addBitmapGraphic(icon: BitmapGraphic) {
		content += BitmapView(icon).node
	}

	private fun getBackState(skin: ButtonSkin): ButtonStateSkin {
		return if (!logic.enabled) {
			skin.disabled
		} else if (logic.pressed) {
			skin.pressed
		} else if (logic.focused) {
			skin.focused
		} else {
			skin.normal
		}
	}

	private fun refreshTextSkin() {
		val skin = skin ?: return
		val stateSkin = getBackState(skin)
		node.placement.gap = skin.gap
		node.placement.border = skin.border
		node.properties.set(stateSkin.text)

		skin.contentOffsetOnPress?.let { offset ->
			if (logic.pressed) {
				content.placement.border = SideIndents(offset.x, offset.y, 0, 0)
			} else {
				content.placement.border = SideIndents(0, 0, offset.x, offset.y)
			}
		}
	}

	override fun applySkin(skin: ButtonSkin) {
		graphic.skin = skin
		refreshTextSkin()
	}
}