package casperix.ui.component

import casperix.math.vector.Vector2d
import casperix.misc.clamp
import casperix.misc.max
import casperix.misc.min
import casperix.misc.toPrecision
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.button.ButtonView
import casperix.ui.component.text.TextView
import casperix.ui.component.texteditor.TextInput
import casperix.ui.component.texteditor.TextInputLogic
import casperix.ui.core.SizeMode
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign
import kotlin.math.roundToInt

class NumberEditorOptions<Number>(
	val decrease: (Number) -> Number,
	val increase: (Number) -> Number,
	val custom: (Number) -> Number,
	val printer: (Number) -> String,
	val parser: (String) -> Number,
)

class NumberEditor<Number>(name: String, initialValue: Number, val options: NumberEditorOptions<Number>) : UIComponent(UINode()) {
	companion object {
		fun createDoubleIncrementer(name: String, precision: Int = 3, initial: Double = 0.0, inc: Double = 1.0, minValue: Double = Double.NEGATIVE_INFINITY, maxValue: Double = Double.POSITIVE_INFINITY, onValue: ((Double) -> Unit)? = null): NumberEditor<Double> {
			val editor = NumberEditor(name, initial, NumberEditorOptions({ max(minValue, it - inc) }, { min(maxValue, it + inc) }, { it.clamp(minValue, maxValue) }, { it.toPrecision(precision) }, { it.toDouble() }))
			onValue?.let { editor.observer.then(onValue) }
			return editor
		}

		fun createDoubleMultiplier(name: String, precision: Int = 3, initial: Double = 0.0, multiplier: Double = 1.1, minValue: Double = Double.NEGATIVE_INFINITY, maxValue: Double = Double.POSITIVE_INFINITY, onValue: ((Double) -> Unit)? = null): NumberEditor<Double> {
			val editor = NumberEditor(name, initial, NumberEditorOptions({ max(minValue, it / multiplier) }, { min(maxValue, it * multiplier) }, { it.clamp(minValue, maxValue) }, { it.toPrecision(precision) }, { it.toDouble() }))
			onValue?.let { editor.observer.then(onValue) }
			return editor
		}

		fun createFloatMultiplier(name: String, precision: Int = 3, initial: Float = 0f, multiplier: Float = 1.1f, minValue: Float = Float.NEGATIVE_INFINITY, maxValue: Float = Float.POSITIVE_INFINITY, onValue: ((Float) -> Unit)? = null): NumberEditor<Float> {
			val editor = NumberEditor(name, initial, NumberEditorOptions({ max(minValue, it / multiplier) }, { min(maxValue, it * multiplier) }, { it.clamp(minValue, maxValue) }, { it.toPrecision(precision) }, { it.toFloat() }))
			onValue?.let { editor.observer.then(onValue) }
			return editor
		}

		fun createFloatIncrementer(name: String, precision: Int = 3, initial: Float = 0f, inc: Float = 1f, minValue: Float = Float.NEGATIVE_INFINITY, maxValue: Float = Float.POSITIVE_INFINITY, onValue: ((Float) -> Unit)? = null): NumberEditor<Float> {
			val editor = NumberEditor(name, initial, NumberEditorOptions({ max(minValue, it - inc) }, { min(maxValue, it + inc) }, { it.clamp(minValue, maxValue) }, { it.toPrecision(precision) }, { it.toFloat() }))
			onValue?.let { editor.observer.then(onValue) }
			return editor
		}

		fun createIntIncrementer(name: String, initial: Int = 0, inc: Int = 1, minValue: Int = Int.MIN_VALUE, maxValue: Int = Int.MAX_VALUE, onValue: ((Int) -> Unit)? = null): NumberEditor<Int> {
			val editor = NumberEditor(name, initial, NumberEditorOptions({ max(minValue, it - inc) }, { min(maxValue, it + inc) }, { it.clamp(minValue, maxValue) }, { it.toString() }, { it.toInt() }))
			onValue?.let { editor.observer.then(onValue) }
			return editor
		}

		fun createIntMultiplier(name: String, initial: Int = 0, multiplier: Float = 1.1f, minValue: Int = Int.MIN_VALUE, maxValue: Int = Int.MAX_VALUE, onValue: ((Int) -> Unit)? = null): NumberEditor<Int> {
			val editor = NumberEditor(name, initial, NumberEditorOptions({ max(minValue, (it / multiplier).roundToInt()) }, { min(maxValue, (it * multiplier).roundToInt()) }, { it.clamp(minValue, maxValue) }, { it.toString() }, { it.toInt() }))
			onValue?.let { editor.observer.then(onValue) }
			return editor
		}
	}

	private val buttonSize = Vector2d(30.0)
	private val textSize = Vector2d(80.0, 30.0)
	val observer = StorageSignal(initialValue)

	var value
		get() = observer.value
		set(value) {
			observer.value = value
		}

	init {
		val allowedSymbols = if (initialValue is Int) {
			"-0123456789"
		} else if (initialValue is Float || initialValue is Double) {
			"-.0123456789"
		} else {
			null
		}

		val some = UINode()
		val valueItem = TextInput(TextInputLogic(TextView("", false), some, allowedSymbols = allowedSymbols), some)
		valueItem.logic.onEnter.then {
			valueItem.logic.onActive.value = false
		}
		valueItem.logic.onActive.then {
			if (!it) {
				parseValue(valueItem)?.let { value ->
					observer.value = value
				}
			}
		}
		observer.then {
			valueItem.logic.onText.value = options.printer(it)
		}
		val valueNode = UINode(textSize)
		valueNode += valueItem

		val decrease = ButtonView("-", buttonSize) {
			observer.value = options.decrease(observer.value)
		}.node
		decrease.placement.align = LayoutAlign.LEFT
		val increase = ButtonView("+", buttonSize) {
			observer.value = options.increase(observer.value)
		}.node
		increase.placement.align = LayoutAlign.RIGHT

		node.layout = Layout.LEFT
		node.placement.align = LayoutAlign.RIGHT
		node.placement.sizeMode = SizeMode.children
		node += valueNode
		node += increase
		node += decrease
		node += TextView(name)
	}

	private fun parseValue(input: TextInput): Number? {
		try {
			val rawValue = input.logic.text
			val value = options.parser(rawValue)
			val limited = options.custom(value)
			return limited
		} catch (_: Exception) {
			return null
		}

	}
}