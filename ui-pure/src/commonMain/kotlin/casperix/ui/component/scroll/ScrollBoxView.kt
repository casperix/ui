package casperix.ui.component.scroll

import casperix.ui.component.ViewComponent
import casperix.ui.core.UINode
import casperix.ui.input.TouchCatcherFactory
import casperix.ui.layout.CustomLayout
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutSide

@Deprecated(message = "Use ScrollBoxView")
typealias Scroller = ScrollBoxView

class ScrollBoxView(node: UINode = UINode()) : ViewComponent<ScrollerSkin>(ScrollerSkin::class, node) {
	private val back = UINode()
	private val front = UINode()
	val content = UINode()
	val logic = ScrollerLogic(node, content)

	init {
		if (node.name == null) node.name = "scroll"
		node += back
		node += content
		node += ScrollerShadow(logic, content, LayoutSide.LEFT)
		node += ScrollerShadow(logic, content, LayoutSide.RIGHT)
		node += ScrollerShadow(logic, content, LayoutSide.TOP)
		node += ScrollerShadow(logic, content, LayoutSide.BOTTOM)
		node += front
		front.touchFilterBuilder = TouchCatcherFactory::never

		ScrollerInput(logic)

		node.layout = CustomLayout(mapOf(Pair(content, null)))
		node.clippingContent = true
	}

	override fun applySkin(skin: ScrollerSkin) {
		back.graphic = skin.back
		front.graphic = skin.front

		content.placement.border = skin.border
	}

	companion object {

		/**
		 * 	Создает скроллер размеры которого определяются заданной нодой
		 *
		 * 	Размеры контента определяются содержащимися детьми.
		 *
		 * 	Функция возвращает контент
		 */
		fun viewport(scrollContainer: UINode, contentLayout: Layout): UINode {
			val scroller = ScrollBoxView(UINode())
			scrollContainer += scroller

			val content = scroller.content
			content.layout = contentLayout
			return content
		}
	}
}