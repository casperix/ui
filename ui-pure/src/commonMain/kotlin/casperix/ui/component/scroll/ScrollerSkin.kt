package casperix.ui.component.scroll

import casperix.math.interpolation.InterpolateTypeFunctiond
import casperix.math.vector.Vector2d
import casperix.ui.core.SideIndents
import casperix.ui.graphic.BitmapGraphic

data class ScrollerAnimationSettings(val interpolator:InterpolateTypeFunctiond<Vector2d>, val durationByDest:(Double)->Double)


data class ScrollerSkin(val border:SideIndents, val shadowSize:Vector2d, val back:BitmapGraphic?, val front: BitmapGraphic?, val shadowGraphic:BitmapGraphic?, val animationSettings: ScrollerAnimationSettings)