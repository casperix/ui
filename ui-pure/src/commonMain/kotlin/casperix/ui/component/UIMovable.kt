package casperix.ui.component

import casperix.misc.disposeAll
import casperix.misc.mutableDisposableListOf
import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class UIMovable(node: UINode) : UIComponent(node) {
	val dragging = StorageSignal<Boolean> (false)

	private var startPosition: Vector2d? = null
	private val movingComponents = mutableDisposableListOf()

	init {
		node.inputs.onTouchDown.then(components) {
			if (it.captured) return@then
			it.captured = true

			dragging.set(true)
			startPosition = node.placement.position - it.position
			node.moveToFront()

			node.root.inputs.onTouchDragged.then(movingComponents) {
				nextPosition(it.position)
			}
		}

		node.root.inputs.onTouchUp.then(components) {
			movingComponents.disposeAll()
			nextPosition(it.position)
			dragging.set(false)
			startPosition = null
		}
	}

	private fun nextPosition(position: Vector2d) {
		startPosition?.let { startPosition ->
			node.placement.setArea(startPosition + position, node.placement.size)
		}
	}
}