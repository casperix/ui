package casperix.ui.component.frame

import casperix.ui.component.text.TextSkin
import casperix.ui.graphic.Graphic

class FrameSkin(
	val titleText: TextSkin,
	val titleHeight: Double,
	val titleBack: Graphic?,
	val contentBack: Graphic?,
) 