package casperix.ui.component.frame

import casperix.math.vector.Vector2d
import casperix.ui.component.ViewComponent
import casperix.ui.component.text.TextView
import casperix.ui.core.*
import casperix.ui.layout.DividerLayout
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutSide

class FrameView(node: UINode = UINode(), val content: UINode = UINode()) : ViewComponent<FrameSkin>(FrameSkin::class, node) {
	val label = TextView("")

	private val contentContainer = UINode()
	private val titleContainer = UINode()

	constructor(constSize: Vector2d, title: String = "frame", content: UINode = UINode()) : this(UINode(), content) {
		this.title = title
		node.placement.sizeMode = SizeMode.const(constSize)
	}

	constructor(title: String, contentLayout: Layout) : this(UINode(), UINode(layout = contentLayout)) {
		this.title = title
	}

	constructor(constSize: Vector2d, title: String, contentComponent: UIComponent) : this(constSize, title, contentComponent.node)

	init {
		if (node.name == null) node.name = "frame"
		titleContainer += label
		contentContainer += content

		titleContainer.placement.sizeMode = SizeMode(MaxChildrenOrViewDimension, ChildrenDimension)
		contentContainer.placement.sizeMode = SizeMode.max

		node += titleContainer
		node += contentContainer
		node.layout = DividerLayout(LayoutSide.TOP)
	}

	var title: String
		get() = label.text
		set(value) {
			label.text = value
		}


	override fun applySkin(skin: FrameSkin) {
		titleContainer.graphic = skin.titleBack
		titleContainer.properties.set(skin.titleText)
		contentContainer.graphic = skin.contentBack
	}
}
