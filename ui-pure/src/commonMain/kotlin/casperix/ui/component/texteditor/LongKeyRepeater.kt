package casperix.ui.component.texteditor

import casperix.input.KeyButton
import casperix.input.KeyDown
import casperix.input.KeyUp
import casperix.misc.Disposable
import casperix.signals.then
import casperix.ui.component.timer.TimerLogic
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class LongKeyRepeaterSettings(val repeatInterval: Double, val repeatDelay: Double) 

/**
 * 	Эмулирует повторное нажатие клавишы, при долгом нажатии
 */
class LongKeyRepeater(node: UINode) : UIComponent(node) {
	private var timerSlot:Disposable? = null

	val settings: LongKeyRepeaterSettings? get() = node.properties.get(LongKeyRepeaterSettings::class)
	private var generating = false

	init {
		node.inputs.onKeyDown.then(components) { event ->
			if (!generating) {
				val settings = settings ?: return@then
				timerSlot?.dispose()
				timerSlot = TimerLogic.timeDelayAndInterval(node, settings.repeatDelay, settings.repeatInterval) {
					generate(event.button)
				}
			}
		}
		node.inputs.onKeyUp.then(components) { event ->
			if (!generating) {
				timerSlot?.dispose()
			}
		}
	}

	private fun generate(button: KeyButton) {
		generating = true
		node.inputs.onKeyDown.set(KeyDown(button))
		node.inputs.onKeyUp.set(KeyUp(button))
		generating = false
	}
}