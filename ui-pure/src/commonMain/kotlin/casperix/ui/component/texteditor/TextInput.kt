package casperix.ui.component.texteditor

import casperix.math.vector.Vector2d
import casperix.signals.then
import casperix.ui.component.timer.TimerLogic
import casperix.ui.layout.CustomLayout
import casperix.ui.component.text.TextView
import casperix.ui.component.text.TextSkin
import casperix.ui.core.*
import casperix.ui.font.Font
import casperix.ui.engine.UIManager
import casperix.ui.engine.GlyphInfo
import casperix.ui.graphic.Graphic
import casperix.ui.layout.Align
import casperix.ui.type.LayoutAlign

data class TextCursorSkin(val blinkInterval: Double, val on: Graphic?, val off: Graphic?)

data class TextInputSkin(val textSkin: TextSkin, val cursor: TextCursorSkin, val normalBack: Graphic, val activeBack: Graphic)

class TextInput(val logic: TextInputLogic, node:UINode = UINode()) : UIComponent(node) {
	val skin: TextInputSkin? get() = node.properties.get(TextInputSkin::class)

	constructor(defaultText: String, node:UINode = UINode())
			: this(TextInputLogic(TextView(defaultText, false,  false), node), node)

	private var blinkTimer:TimerLogic? = null
	private var blink = false


	val textArea = logic.textComponent
	val cursor = UINode()



	init {
		if (node.name == null) node.name = "textInput"
		node.clippingContent = true

		node += textArea
		node.layout = CustomLayout(mapOf(Pair(cursor, null)))

		logic.onCursor.then(components) {
			showAndPlay()
		}

		logic.onText.then(components) { value ->
			textArea.text = value
		}

		logic.onActive.then(components) { active ->
			updateState()
			if (active) {
				showAndPlay()
			} else {
				stop()
			}
		}
		node.events.propertyChanged.then(components) {
			updateState()
		}
	}

	private fun stop() {
		blinkTimer?.dispose()
		updateCursor()
	}

	private fun showAndPlay() {
		val skin = skin ?: return
		blinkTimer?.dispose()
		blink = false
		blinkTimer = TimerLogic.timeInterval(node, skin.cursor.blinkInterval) {
			blink = !blink
			updateCursor()
		}
	}

	private fun updateState() {
		val skin = skin ?: return

		node.properties.set(skin.textSkin)

		val metrics = skin.textSkin.font.metrics
		val lineHeight = metrics.ascent + metrics.descent

		textArea.node.placement.align = LayoutAlign.CENTER_CENTER
		textArea.node.placement.sizeMode = SizeMode(MaxChildrenOrViewDimension, ConstDimension(lineHeight))
		if (logic.onActive.value) {
			node.graphic = skin.activeBack
			node += cursor
		} else {
			node.graphic = skin.normalBack
			node -= cursor
		}
	}

	private fun updateCursor() {
		val skin = skin ?: return
		val cursorGraphic = (if (blink) skin.cursor.on else skin.cursor.off) ?: return
		val area = getCursorArea() ?: return
		if (!logic.onActive.value) return

		cursor.placement.setArea(area.position.round(), area.size)
		cursor.graphic = cursorGraphic
	}

	private class CursorArea(val position: Vector2d, val size: Vector2d)

	private fun getCursorArea(): CursorArea? {
		val engine = engine ?: return null
		val textSkin = textArea.skin ?: return null

		val textLayout = TextHelper.getTextLayout(textArea, engine) ?: return null
		val fontInfo = TextHelper.getFontInfo(textArea) ?: return null

		val cursorSize = Vector2d(2.0, fontInfo.metrics.ascent)
		val cursorIndex = logic.onCursor.value
		val layoutOffset = textArea.node.placement.getBound().margin.leftTop

		val maxPosition = textArea.node.placement.size
		textLayout.glyphs.forEach { nextGlyph ->
			if (nextGlyph.symbolIndex == cursorIndex) {
				return placeCursorBeforeGlyph(nextGlyph, layoutOffset, cursorSize, fontInfo, maxPosition)
			}
		}

		val lastGlyph = textLayout.glyphs.lastOrNull()
		if (lastGlyph != null) {
			return placeCursorAfterGlyph(lastGlyph, layoutOffset, cursorSize, fontInfo, maxPosition)
		}

		val virtualGlyph = createVirtualGlyph(engine, textSkin)
		return placeCursorBeforeGlyph(virtualGlyph, layoutOffset, cursorSize, fontInfo, maxPosition)
	}

	private fun placeCursorBeforeGlyph(glyph: GlyphInfo, layoutOffset: Vector2d, cursorSize: Vector2d, fontInfo: Font, maxPosition: Vector2d): CursorArea {
		return CursorArea(layoutOffset + (glyph.position + Vector2d(-cursorSize.x / 2.0, fontInfo.metrics.descent)).lower(maxPosition), cursorSize)
	}

	private fun placeCursorAfterGlyph(glyph: GlyphInfo, layoutOffset: Vector2d, cursorSize: Vector2d, fontInfo: Font, maxPosition: Vector2d): CursorArea {
		return CursorArea(layoutOffset + (glyph.position + Vector2d(glyph.offset.x + glyph.size.x, fontInfo.metrics.descent)).lower(maxPosition), cursorSize)
	}


	private fun createVirtualGlyph(engine: UIManager, textSkin: TextSkin): GlyphInfo {
		val textLayout = engine.drawer.layout(textSkin.font, "W", textArea.node.placement.size, Align.MIN, Align.MIN, textArea.wordWrap)
		return textLayout.glyphs.last()
	}

}

