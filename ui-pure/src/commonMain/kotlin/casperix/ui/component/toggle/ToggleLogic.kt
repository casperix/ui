package casperix.ui.component.toggle

import casperix.signals.concrete.BooleanSwitcher
import casperix.signals.switch
import casperix.signals.then
import casperix.ui.component.button.ButtonLogic
import casperix.ui.core.UIComponent


class ToggleLogic(val button: ButtonLogic, val switch: BooleanSwitcher) : UIComponent(button.node) {
	init {
		button.onClicked.then {
			switch.switch()
		}
	}

	fun addListener(listener: (Boolean) -> Unit) {
		switch.then(components, listener)
	}

}