package casperix.ui.component.console

class ConsoleCommand(
	val name: String,
	val description:String,
	val action: (List<String>) -> String,
)