package casperix.ui.component

import casperix.signals.concrete.StoragePromise
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class VisibleLogic(container: UINode, val content: UINode, val onVisible: StoragePromise<Boolean> = StorageSignal(false)) : UIComponent(container) {

	init {
		onVisible.then(components) { update() }
	}

	private fun update() {
		val show = onVisible.value
		if (show) {
			node += content
		} else {
			node -= content
		}

	}
}