package casperix.ui.component.chart

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.math.vector.toQuad
import casperix.ui.component.text.TextSkin
import casperix.ui.engine.Drawer
import casperix.ui.graphic.Graphic

class LineChartGraphic(val textSkin:TextSkin, val diagram: LineChartSource, val boxSettings: ChartBoxSettings, val chartSettings: ChartSettings) : Graphic {
	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		val transform = ChartCanvasTransform(textSkin, boxSettings, position, size)
		val points = diagram.points


		val lineSettings = chartSettings.lineSettings
		if (lineSettings != null) {
			for (lineId in 1 until points.size) {
				val A = transform.chartToScreen(points[lineId - 1])
				val B = transform.chartToScreen(points[lineId])
				drawer.drawLine(A, B, lineSettings.color, lineSettings.thick)
			}
		}
		val pointSettings = chartSettings.pointSettings
		if (pointSettings != null) {
			points.forEach {
				val pointPosition = transform.chartToScreen(it)
				drawer.drawQuad(Box2d.byRadius(pointPosition, pointSettings.size / 2.0).toQuad(), pointSettings.color)
			}
		}

	}
}