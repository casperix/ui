package casperix.ui.component.chart

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.ui.type.LayoutSide

interface ChartSource

class LineChartSource(val points: List<Vector2d>) : ChartSource

/**
 * 	@param chartArea -- размер области для вывода графиков (в пикселях)
 * 	@param sourceArea -- используются данные из этой области (исходные координаты)
 */
class ChartBoxSettings(
	val name: String? = null,
	val legendSide: LayoutSide = LayoutSide.LEFT,
	val axisLabelSize: Vector2d = Vector2d(60.0, 30.0),
	val xAxis: ChartAxisSettings = ChartAxisSettings("", 5.0),
	val yAxis: ChartAxisSettings = ChartAxisSettings("", 5.0),
	val showAxisName: Boolean = true,
	val yDown: Boolean = false,
	val gridSettings: ChartGridSettings = ChartGridSettings(),
	val sourceArea: Box2d = Box2d(-Vector2d(10.0), Vector2d(10.0)),
	val legendMarkerSize: Vector2d = Vector2d(40.0, 20.0),
)

class ChartAxisSettings(val name:String, val markerInterval: Double, val markerPointSize:Double? = 8.0, val valuePrecision: Int = 0, val line:ChartLineSettings = ChartLineSettings(Color.WHITE.toColor3d().expand(1.0), 2.0), val textColor: Color4d = Color4d(0.0, 0.0, 0.0, 1.0))

class ChartGridSettings(val lineSettings: ChartLineSettings = ChartLineSettings(Color.WHITE.toColor3d().expand(0.1), 2.0))

class ChartSettings(val lineSettings: ChartLineSettings?, val pointSettings:ChartPointSettings?)

class ChartLineSettings(val color: Color4d, val thick: Double = 2.0)
class ChartPointSettings(val color: Color4d, val size:Vector2d = Vector2d(6.0))

