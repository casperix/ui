package casperix.ui.component.chart

import casperix.misc.toPrecision
import casperix.math.vector.Vector2d
import casperix.math.geometry.Line2d
import casperix.math.color.Color4d
import casperix.ui.layout.Orientation

class ChartCanvasScheme(val transform: ChartCanvasTransform) {
	class GridLine(val line: Line2d, val sourceValue: Double, val orientation: Orientation)
	class ChartLabel(val center: Vector2d, val size:Vector2d, val text: String, val color: Color4d)

	val grids = mutableListOf<GridLine>()
	val labels = mutableListOf<ChartLabel>()

	init {
		val settings = transform.boxSettings
		val sourceArea = settings.sourceArea
		calculateGrids(transform, sourceArea.min.x, sourceArea.max.x, settings.xAxis.markerInterval, Orientation.VERTICAL)
		calculateGrids(transform, sourceArea.min.y, sourceArea.max.y, settings.yAxis.markerInterval, Orientation.HORIZONTAL)

		val legendHalfWidth = settings.axisLabelSize.x / 2.0
		val legendHalfHeight = settings.axisLabelSize.y / 2.0

		grids.forEach {
			val axisSettings = if (it.orientation == Orientation.HORIZONTAL) {
				settings.yAxis
			} else {
				settings.xAxis
			}
			val value = it.sourceValue.toPrecision(axisSettings.valuePrecision)
			val textPosition = if (it.orientation == Orientation.HORIZONTAL) {
				it.line.v0 - Vector2d(legendHalfWidth, 0.0)
			} else {
				it.line.v0 + Vector2d(0.0, legendHalfHeight)
			}

			labels += ChartLabel(textPosition, settings.axisLabelSize, value, axisSettings.textColor)
		}

		if (settings.showAxisName) {
			setupAxisName(settings.axisLabelSize, settings.xAxis, transform.nodePosition + transform.nodeSize + Vector2d(legendHalfWidth, 0.0))
			setupAxisName(settings.axisLabelSize, settings.yAxis, transform.nodePosition + Vector2d(0.0, -legendHalfHeight))
		}

		if (settings.name != null) {
			val size = Vector2d(transform.nodeSize.x, settings.axisLabelSize.y)
			labels += ChartLabel(transform.nodePosition + transform.nodeSize.xAxis / 2.0 + Vector2d(0.0, -legendHalfHeight), size, settings.name, transform.textSkin.color.toColor4d())
		}
	}

	private fun setupAxisName(axisLabelSize: Vector2d, nAxis: ChartAxisSettings, namePosition: Vector2d) {
		labels += ChartLabel(namePosition, axisLabelSize, nAxis.name, nAxis.textColor)
	}

	private fun calculateGrids(scheme: ChartCanvasTransform, start: Double, finish: Double, interval: Double, orientation: Orientation) {
		var offset = start + interval - (if (start >= 0.0) start else (interval - start)) % interval

		while (offset <= finish) {
			val line = getGridLine(orientation, scheme, (offset - start) / (finish - start))
			grids += GridLine(line, offset, orientation)
			offset += interval
		}
	}

	private fun getGridLine(orientation: Orientation, scheme: ChartCanvasTransform, relativePosition: Double): Line2d {
		val sourceArea = scheme.boxSettings.sourceArea

		val chartLine = if (orientation == Orientation.VERTICAL) {
			val offset = sourceArea.min.x + sourceArea.dimension.x * relativePosition
			val A = Vector2d(offset, sourceArea.min.y)
			val B = Vector2d(offset, sourceArea.max.y)
			Line2d(A, B)
		} else {
			val offset = sourceArea.min.y + sourceArea.dimension.y * relativePosition
			val A = Vector2d(sourceArea.min.x, offset)
			val B = Vector2d(sourceArea.max.x, offset)
			Line2d(A, B)
		}

		return chartLine.convert { scheme.chartToScreen(it) }
	}
}