package casperix.ui.component.bitmap

import casperix.math.vector.Vector2d
import casperix.signals.concrete.EmptySignal
import casperix.signals.then
import casperix.ui.component.text.TextView
import casperix.ui.core.SizeMode
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.engine.Bitmap
import casperix.ui.graphic.*
import casperix.ui.type.LayoutAlign

@Deprecated(message = "Use BitmapView")
typealias Image = BitmapView

class BitmapView(graphic: BitmapGraphic?, scalingDependency: Boolean, node: UINode = UINode()) : UIComponent(node) {

	constructor(graphic: BitmapGraphic?, preferredSize: Vector2d) : this(graphic, false) {
		node.placement.sizeMode = SizeMode.const(preferredSize)
	}

	constructor(graphic: BitmapGraphic?) : this(graphic, true)

	constructor(bitmap:Bitmap, preferredSize: Vector2d) : this(BitmapGraphic(bitmap), false) {
		node.placement.sizeMode = SizeMode.const(preferredSize)
	}

	constructor(bitmap:Bitmap) : this(BitmapGraphic(bitmap), true)


	val onChanged = EmptySignal()
	var scalingDependency: Boolean by onChanged.observable(scalingDependency)

	init {
		if (node.name == null) node.name = "image"
		node.placement.align = LayoutAlign.CENTER_CENTER
		node.graphic = graphic

		onChanged.then(components) {
			update()
		}
		node.events.onGraphic.then (components) {
			update()
		}
		node.events.propertyChanged.then(components) {
			update()
		}
	}

	private fun update() {
		val graphic = node.graphic
		if (scalingDependency && graphic is BitmapGraphic) {
			val scaling = graphic.scaling
			val bitmap = graphic.bitmap
			node.placement.sizeMode = when(scaling) {
				is ScalingNone->SizeMode.const(bitmap.size.toVector2d())
				is ScalingUniform ->SizeMode.const(bitmap.size.toVector2d() * scaling.scale)
				is ScalingAdaptive ->SizeMode.view
				is ScalingStretch ->SizeMode.view
				is Scaling9Slice ->SizeMode.view
			}
		}
	}
}

