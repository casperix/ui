package casperix.ui.component.tab

import casperix.signals.concrete.StoragePromise
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

interface AbstractTab {
	val switch: StoragePromise<Boolean>
	val button: UINode
	val content: UINode
}

open class ToggleTab(val toggle: ToggleView, override val content: UINode) : AbstractTab {
	override val switch = toggle.logic.switch
	override val button = toggle.node

}

class CustomTab<Component : UIComponent>(toggle: ToggleView, val component: Component) : ToggleTab(toggle, component.node)