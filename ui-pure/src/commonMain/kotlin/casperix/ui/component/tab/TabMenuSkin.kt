package casperix.ui.component.tab

import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.graphic.Graphic
import casperix.ui.layout.Align

data class SidedTabMenuSkin(
	val button:ToggleSkin,
	val mainBack:Graphic?,
	val headerBack:Graphic?,
	val buttonsBack:Graphic?,
	val contentBack: Graphic?,
)
data class TabMenuSkin(
	val top: SidedTabMenuSkin,
	val bottom: SidedTabMenuSkin,
	val left: SidedTabMenuSkin,
	val right: SidedTabMenuSkin,
	val buttonsAlign: Align,
	val buttonGap:Double,
)