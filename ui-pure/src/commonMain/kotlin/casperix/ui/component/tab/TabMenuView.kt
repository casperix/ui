package casperix.ui.component.tab

import casperix.signals.collection.ObservableMutableList
import casperix.signals.collection.observableListOf
import casperix.signals.collection.toObservableList
import casperix.signals.then
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.core.SizeMode
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.DividerLayout
import casperix.ui.layout.LineLayout
import casperix.ui.type.LayoutSide
import casperix.ui.util.BooleanGroupSelector

@Deprecated(message = "Use TabMenuView")
typealias TabMenu<CustomTab > = TabMenuView<CustomTab >

class TabMenuView<CustomTab : AbstractTab>(
	val buttonSide: LayoutSide,
	val behaviour: BooleanGroupSelector.Behaviour = BooleanGroupSelector.Behaviour.ALWAYS_ONE,
	val tabs: ObservableMutableList<CustomTab> = observableListOf(),
	node: UINode = UINode(),
) : UIComponent(node) {
	private var selector: BooleanGroupSelector? = null


	val skin: TabMenuSkin? get() = node.properties.get(TabMenuSkin::class)

	private val buttons = UINode()
	val header = UINode()
	val content = UINode()

	init {
		if (node.name == null) node.name = "tabMenu"
		header += buttons
		node.directDrawChildren = false
		node += header
		node += content

		refreshComponentLayout()

		tabs.forEach {
			addTab(it)
		}

		tabs.then(components, {
			addTab(it)
		}, {
			removeTab(it)
		})

		node.events.propertyChanged.then(components) {
			refreshSkin()
		}
	}

	private fun refreshComponentLayout() {
		node.layout = DividerLayout(buttonSide)
		buttons.layout = LineLayout(buttonSide.orientation().rotate())
		buttons.placement.sizeMode = SizeMode.children
	}

	private fun removeTab(tab: AbstractTab) {
		val button = tab.button
		buttons.children -= button
		content -= tab.content
		rebuildSelector()
	}

	fun getSelectedTab(): CustomTab? {
		tabs.forEach {
			if (it.switch.value) return it
		}
		return null
	}

	private fun addTab(tab: CustomTab) {
		val visibleLogic = TabVisibleLogic(tab, buttons, content)
		visibleLogic.tab.switch.then(components) {
			refreshSelected()
		}
		rebuildSelector()
		refreshSkin()
	}

	private fun refreshSelected() {
		val selected = getSelectedTab()

		content.children.clear()
		if (selected != null) {
			content += selected.content
		}

		val contentBack = if (getSelectedTab() == null) {
			null
		} else {
			val skin = skin ?: return
			getSidedTabMenuSkin(skin).contentBack
		}
		content.graphic = contentBack
	}

	override fun dispose() {
		super.dispose()
		selector?.dispose()
	}

	private fun rebuildSelector() {
		selector?.dispose()
		selector = BooleanGroupSelector(tabs.map { it.switch }.toObservableList(), behaviour)
	}

	private fun getSidedTabMenuSkin(skin: TabMenuSkin): SidedTabMenuSkin {
		return when (buttonSide) {
			LayoutSide.TOP -> skin.top
			LayoutSide.BOTTOM -> skin.bottom
			LayoutSide.LEFT -> skin.left
			LayoutSide.RIGHT -> skin.right
		}
	}

	private fun refreshButtonSkin(tab: AbstractTab, skin: TabMenuSkin) {
		if (tab is ToggleTab) {
			val buttonSkin = getSidedTabMenuSkin(skin).button
			tab.toggle.node.properties.set(buttonSkin)
			if (tab.switch.value) {
				refreshSelected()
			}
		}
	}

	private fun refreshSkin() {
		val skin = skin ?: return

		val sideSkin = getSidedTabMenuSkin(skin)
		node.graphic = sideSkin.mainBack
		header.graphic = sideSkin.headerBack
		buttons.graphic = sideSkin.buttonsBack

		tabs.forEach { tab ->
			refreshButtonSkin(tab, skin)
		}
		refreshComponentLayout()
		refreshSelected()
	}
}