package casperix.ui.component.tree

import casperix.misc.DisposableHolder
import casperix.misc.disposeAll
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.component.button.ButtonLogic
import casperix.ui.component.bitmap.BitmapView
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleLogic
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign


@Deprecated(message = "Use TreeView")
typealias Tree = TreeView

class TreeView(node: UINode = UINode(name = "tree")) : ViewComponent<TreeSkin>(TreeSkin::class, node) {
	private var root = TreeNode(TextView("root").node)
	private val treeListenerHolder = DisposableHolder()
	private var dirtyRender = false
	private var hideRoot = false

	init {
		if (node.name == null) node.name = "tree"
		node.layout = Layout.BOTTOM

		components.add(treeListenerHolder)
		node.events.nextFrame.then(components) { invalidate() }
	}

	constructor(hideRoot: Boolean, root: TreeNode, node: UINode = UINode(name = "tree")) : this(node) {
		setRoot(hideRoot, root)
	}

	fun setRoot(hideRoot: Boolean, root: TreeNode) {
		this.root = root
		this.hideRoot = hideRoot
		invalidateRequest()
	}

	private fun invalidateRequest() {
		dirtyRender = true
	}

	private fun invalidate() {
		if (!dirtyRender) return
		val skin = skin ?: return

		val info = TreeNodeFactory.create(root)

		subscribe(info.visibleList)

		node.children.clear()

		if (hideRoot) {
			appendChildren(skin,  root, 0)
		} else {
			append(skin,  root, 0)
		}

		dirtyRender = false
	}

	private fun createRow(skin: TreeSkin,  treeNode: TreeNode, hierarchyDepth: Int): UINode {
		val rowNode = UINode()
//		rowNode.placement.sizeMode = SizeMode.min

		val toggleSize = skin.toggleSize
		val marker = if (treeNode.children.isNotEmpty()) {
			val toggle = ToggleView(ToggleLogic(ButtonLogic(rowNode), treeNode.opened), UINode(toggleSize))
			toggle.node.placement.align = LayoutAlign.LEFT
			toggle.skin = skin.toggle
			UINode(children = listOf( toggle.node))
		} else if (skin.emptyMarker != null) {
			BitmapView(skin.emptyMarker).node
		} else {
			null
		}

		val tabSize = skin.tabWidth
		if (marker != null) {
			marker.placement.align = LayoutAlign.LEFT
			marker.placement.gap = SideIndents((hierarchyDepth + 1) * tabSize - toggleSize.x, 0.0, 0.0, 0.0)
			rowNode += marker
		}

		val content = UINode()
		content.placement.sizeMode = SizeMode.children
		content.placement.align = LayoutAlign.LEFT
		content.placement.gap = SideIndents((hierarchyDepth + 1) * tabSize, 0.0, 0.0, 0.0)
		content += treeNode.content

		rowNode += content
		return rowNode
	}

	private fun append(skin: TreeSkin, mainNode: TreeNode, hierarchyDepth: Int) {
		node += createRow(skin,  mainNode, hierarchyDepth)

		if (mainNode.opened.value) {
			appendChildren(skin, mainNode, hierarchyDepth + 1)
		}
	}

	private fun appendChildren(skin: TreeSkin, treeNode: TreeNode, hierarchyDepth: Int) {
		treeNode.children.forEach { childNode ->
			append(skin, childNode, hierarchyDepth)
		}
	}

	private fun subscribe(nodes: Collection<TreeNode>) {
		treeListenerHolder.components.disposeAll()
		nodes.forEach { node ->
			node.opened.then(treeListenerHolder.components) { invalidateRequest() }
			node.children.then(treeListenerHolder.components, { invalidateRequest() }, { invalidateRequest() })
		}
	}

	override fun applySkin(skin: TreeSkin) {
		invalidateRequest()
	}


}