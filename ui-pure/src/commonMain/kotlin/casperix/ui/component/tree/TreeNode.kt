package casperix.ui.component.tree

import casperix.signals.collection.ObservableMutableList
import casperix.signals.concrete.StorageSignal
import casperix.ui.core.UINode

class TreeNode(val content: UINode, opened: Boolean = false) {
	val opened = StorageSignal(opened)
	val children = ObservableMutableList<TreeNode>()

	constructor(content: UINode, opened: Boolean, children: Collection<TreeNode>) : this(content, opened) {
		this.children += children
	}
}