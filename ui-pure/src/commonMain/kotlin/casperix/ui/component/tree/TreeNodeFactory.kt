package casperix.ui.component.tree

import casperix.math.vector.Vector2d
import kotlin.math.max

internal class TreeNodeFactory {
	val allNodes = HashSet<TreeNode>()
	val visibleNodes = HashSet<TreeNode>()
	var maxDepth = 0
	var maxWidth = Vector2d(0.0)

	companion object {
		fun create(rootNode: TreeNode): TreeNodeInfo {
			val factory = TreeNodeFactory()
			factory.collectVisibleNodes(rootNode, 0, true)
			return TreeNodeInfo(factory.allNodes, factory.visibleNodes, factory.maxDepth, factory.maxWidth)
		}
	}

	fun collectVisibleNodes(treeNode: TreeNode, depth: Int, visible: Boolean) {
		maxDepth = max(maxDepth, depth)
		maxWidth = maxWidth.upper(treeNode.content.placement.size)

		if (!allNodes.add(treeNode)) throw Error("Tree has recursion, see $treeNode")

		if (visible) {
			visibleNodes.add(treeNode)
		}

		val nextVisible = visible && treeNode.opened.value

		treeNode.children.forEach {
			collectVisibleNodes(it, depth + 1, nextVisible)
		}
	}
}