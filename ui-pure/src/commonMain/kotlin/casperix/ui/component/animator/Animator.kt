package casperix.ui.component.animator

import casperix.misc.disposeAll
import casperix.signals.then
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class Animator<V>(node: UINode, val valueReceiver:(V)->Unit, var interpolator:(V, V, Double)->V) : UIComponent(node) {
	private var time:Double = 0.0

	fun stop() {
		time = 0.0
		components.disposeAll()
	}

	fun start(startValue:V, finishValue:V, duration:Double) {
		stop()

		valueReceiver(startValue)
		node.events.nextFrame.then(components) {
			time += it
			if (time >= duration) {
				time = duration
				components.disposeAll()
			}
			val value = interpolator(startValue, finishValue, time / duration)
			valueReceiver(value)
		}
	}
}