package casperix.ui.component.panel

import casperix.ui.component.ViewComponent
import casperix.ui.core.UINode

@Deprecated(message = "Use PanelView")
typealias Panel = PanelView

class PanelView(node: UINode = UINode()) : ViewComponent<PanelSkin>(PanelSkin::class, node) {

	init {
		if (node.name == null) node.name = "panel"
	}

	override fun applySkin(skin: PanelSkin) {
		node.graphic = skin.graphic
		node.placement.gap = skin.gap
		node.placement.border = skin.border
	}
}
