package casperix.ui.component.panel

import casperix.ui.core.SideIndents
import casperix.ui.graphic.Graphic

class PanelSkin(
	val graphic: Graphic,
	val gap:SideIndents,
	val border:SideIndents,
)