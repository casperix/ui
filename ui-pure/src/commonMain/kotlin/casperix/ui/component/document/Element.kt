package casperix.ui.component.document

import casperix.math.vector.Vector2d
import casperix.math.color.Color
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

sealed class Element

data class TextElement(val text: String, val color: Color? = null, val fontName: String? = null, val fontSize: Int? = null) : Element()
data class ActionElement(val action: String, val text: String) : Element()
data class ImageElement(val source: String, val size: Vector2d) : Element()