package casperix.ui.component.document

import casperix.math.vector.Vector2d
import casperix.ui.component.text.TextSkin
import casperix.ui.engine.Supplier
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.Graphic

class DocumentSkin(val back: Graphic?, val textSkin: TextSkin, val imageSize: Vector2d) {
	val customActions = mutableMapOf<String, () -> Unit>()
	val customImages = mutableMapOf<String, BitmapGraphic>()

	fun getImage(supplier: Supplier, source: String): BitmapGraphic {
		return customImages[source] ?: supplier.image(DocumentParser.parseFileName(source))
	}

	fun getAction(source: String): () -> Unit {
		return customActions[source] ?: { }
	}
}