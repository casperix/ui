package casperix.ui.component.document

import casperix.file.FileReference
import casperix.misc.substringOrEmpty
import casperix.math.vector.Vector2d
import casperix.math.color.ColorCodec
import casperix.ui.source.FontLink

object DocumentParser {

	enum class Tag {
		image,
		color,
		font,
		size,
		action,
	}

	val PROPERTY_DIVIDER = ":"
	val tagReg = Regex("\\[[^]]+]")

	fun parseFileName(fileName: String): FileReference {
		return if (fileName.startsWith("external@")) {
			FileReference.external(fileName.removePrefix("external@"))
		} else if (fileName.startsWith("absolute@")) {
			FileReference.absolute(fileName.removePrefix("absolute@"))
		} else if (fileName.startsWith("classpath@")) {
			FileReference.classpath(fileName.removePrefix("classpath@"))
		} else {
			FileReference.classpath(fileName)
		}
	}

	fun parse(text: String, skin: DocumentSkin): List<Element> {
		return divideBySpace(parseTags(text, skin))
	}

	class Stack<V>(def: V) {
		private val stack = mutableListOf<V>()

		init {
			stack += def
		}

		fun push(next: V) {
			stack += next
		}

		fun pop(): V {
			if (stack.size > 1) {
				stack.removeLast()
			}
			return stack.last()
		}

		fun get(): V {
			return stack.last()
		}
	}

	private fun divideBySpace(blocks: List<Element>): List<Element> {
		val whitespace = Regex("\\s")
		return blocks.flatMap { originalBlock ->
			if (originalBlock is TextElement) {
				val words = whitespace.findAll(originalBlock.text).toList()
				var lastIndex = 0
				val output = mutableListOf<String>()
				words.forEach {
					val nextIndex = it.range.start + 1
					//	word
					if (lastIndex < nextIndex - 1) output += originalBlock.text.substringOrEmpty(lastIndex, nextIndex - 1)
					//	whitespace
					output += originalBlock.text.substringOrEmpty(nextIndex - 1, nextIndex)
					lastIndex = nextIndex
				}
				output += originalBlock.text.substringOrEmpty(lastIndex, originalBlock.text.length)

				output.map { word -> TextElement(word, originalBlock.color, originalBlock.fontName, originalBlock.fontSize) }
			} else {
				listOf(originalBlock)
			}
		}
	}

	private fun parseTags(text: String, skin: DocumentSkin): List<Element> {

		val skinFont = skin.textSkin.font.source as? FontLink

		var lastIndex = 0
		val textColorSource = Stack(skin.textSkin.color)
		val fontNameSource = Stack(skinFont?.file)
		val fontSizeSource = Stack(skinFont?.size)

		val tagBlocks = mutableListOf<Element>()

		val addText: (String) -> Unit = { value ->
			tagBlocks += TextElement(value, textColorSource.get(), fontNameSource.get()?.path, fontSizeSource.get())
		}
		val addLastText: (Int) -> Unit = { nextIndex ->
			if (lastIndex != nextIndex) {
				val subText = text.substring(lastIndex, nextIndex)
				lastIndex = nextIndex
				addText(subText)
			}
		}
		tagReg.findAll(text).forEach {
			addLastText(it.range.start)


			val valueWithoutBrackets = it.value.substring(1, it.value.length - 1)

			val isCloseTag = valueWithoutBrackets.firstOrNull() == '/' || valueWithoutBrackets.lastOrNull() == '/'
			val valueClean = valueWithoutBrackets.removePrefix("/").removeSuffix("/")

			val tagElements = valueClean.split(PROPERTY_DIVIDER)
			val tagName = tagElements.getOrNull(0)
			val tagValue = tagElements.getOrNull(1)
			val tag = Tag.values().firstOrNull { it.name == tagName }

			if (tag == null) {
				addText(it.value)
			} else if (tag == Tag.image) {
				if (tagValue != null) {
					val sizeX = tagElements.getOrNull(2)?.toDoubleOrNull() ?: skin.imageSize.x
					val sizeY = tagElements.getOrNull(3)?.toDoubleOrNull() ?: skin.imageSize.y
					tagBlocks += ImageElement(tagValue, Vector2d(sizeX, sizeY))
				}
			} else if (tag == Tag.action) {
				if (tagValue != null) {
					val content = getTagContent(tagElements, 2)
					tagBlocks += ActionElement(tagValue, content ?: tagValue)
				}
			} else {
				if (tagValue != null) {
					if (tag == Tag.color) textColorSource.push(ColorCodec.parseHex(tagValue))
					if (tag == Tag.font) fontNameSource.push(parseFileName(tagValue))
					if (tag == Tag.size) fontSizeSource.push(tagValue.toIntOrNull())
				}

				getTagContent(tagElements, 2)?.let { content ->
					addText(content)
				}

				if (isCloseTag) {
					if (tag == Tag.color) textColorSource.pop()
					if (tag == Tag.font) fontNameSource.pop()
					if (tag == Tag.size) fontSizeSource.pop()
				}
			}


			lastIndex = it.range.last + 1
		}

		addLastText(text.length)

		return tagBlocks
	}

	private fun getTagContent(tagElements: List<String>, startIndex: Int): String? {
		if (tagElements.size <= startIndex) return null
		return tagElements.subList(startIndex, tagElements.size).joinToString(PROPERTY_DIVIDER)

	}
}