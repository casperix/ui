package casperix.ui.component.text

import casperix.math.vector.Vector2d
import casperix.math.color.Color
import casperix.ui.core.SideIndents
import casperix.ui.font.Font

data class TextShadowSettings(val offset: Vector2d, val color: Color)

data class TextSkin(val font: Font, val gap:SideIndents, val color: Color = Color.WHITE, val bold:Boolean = false, val italic:Boolean = false, val underline:Boolean = false, val shadow: TextShadowSettings? = null)