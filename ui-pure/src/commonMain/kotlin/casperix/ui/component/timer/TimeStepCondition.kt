package casperix.ui.component.timer

class TimeStepCondition(val interval: Double) : TimerCondition {
	private var remainsTime = interval

	init {
		if (interval <= 0.0 || !interval.isFinite()) throw Error("Skip time must be positive number")
	}

	override fun check(tick: Double): Boolean {
		remainsTime -= tick
		return if (remainsTime <= 0.0) {
			remainsTime += interval
			true
		} else {
			false
		}
	}

}