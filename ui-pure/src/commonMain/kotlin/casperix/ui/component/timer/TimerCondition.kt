package casperix.ui.component.timer

interface TimerCondition {
	fun check(tick:Double):Boolean
}

