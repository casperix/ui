package casperix.ui.component

import casperix.math.function.FunctionStatistic
import casperix.misc.toPrecision
import casperix.ui.component.text.TextView
import casperix.ui.component.timer.TimerLogic

object FPSCounter {
	fun create(): TextView {
		val text = TextView("")
		val fpsCounter = FunctionStatistic(32)

		text.node.events.nextFrame.then {
			val fps = 1.0 / it
			fpsCounter.add(fps)
		}
		TimerLogic.timeInterval(text.node, 0.1) {
			text.text = fpsCounter.average.toPrecision(1)
		}
		return text
	}
}