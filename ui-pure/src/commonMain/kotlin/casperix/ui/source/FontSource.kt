package casperix.ui.source

import casperix.file.FileReference

interface FontSource

data class FontLink(val file: FileReference, val size: Int) : FontSource

data class FontMultiLink(val links: List<FontLink>) : FontSource
