package casperix.ui.source

import casperix.file.FileReference
import casperix.map2d.IntMap2D


interface BitmapSource

data class BitmapFile(val file: FileReference) : BitmapSource
data class BitmapData(val map: IntMap2D) : BitmapSource
data class BitmapEncoded(val encoded: ByteArray) : BitmapSource
