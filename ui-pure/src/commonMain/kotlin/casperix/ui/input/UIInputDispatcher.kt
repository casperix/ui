package casperix.ui.input

import casperix.input.*
import casperix.math.vector.Vector2d
import casperix.ui.core.UINode

class UIInputDispatcher(val root: UINode, val dispatcher: InputDispatcher) {
	val cursorFocusController = ContinuousEventController()
	val touchSelectController = ContinuousEventController()

	init {
		dispatcher.onKeyDown.then { dispatchKeyEvent(it) }
		dispatcher.onKeyUp.then { dispatchKeyEvent(it) }
		dispatcher.onKeyTyped.then { dispatchKeyEvent(it) }

		dispatcher.onTouchUp.then { dispatchTouchEvent(it) }
		dispatcher.onTouchDragged.then { dispatchTouchEvent(it) }
		dispatcher.onTouchDown.then { dispatchTouchEvent(it) }
		dispatcher.onMouseWheel.then { dispatchTouchEvent(it) }
		dispatcher.onMouseMove.then { dispatchTouchEvent(it) }
	}

	companion object {
		var isDebugMode = false
	}
	private fun dispatchTouchEvent(event: PointerEvent): Boolean {
		return dispatchTouchEvent(event, root)
	}

	fun dispatchTouchEvent(event: PointerEvent, first:UINode): Boolean {
		val position = event.position
		val caught = catchPointerEvent(first, event) ?: first

		setupHierarchyFocus(caught, position)

		if (event is TouchDown) {
			setupHierarchySelect(caught, position)
		}

		sendHierarchyPointerEvent(caught, event)

		if (isDebugMode && (event is TouchDown || event is TouchUp)) {
			println("$event => $caught => ${event.captured}")
		}
		val hasFocused = cursorFocusController.items().size > 1;//not only root
		event.captured = event.captured || hasFocused
		return event.captured
	}

	private fun dispatchKeyEvent(event: KeyEvent): Boolean {
		var keyCatcherList = touchSelectController.items()
		if (keyCatcherList.isEmpty()) keyCatcherList = listOf(root)

		if (isDebugMode) {
			println("$event => ${keyCatcherList.last()}")
		}

		keyCatcherList.forEach {
			sendKeyboardEvent(it, event)
		}
		return event.captured
	}

	private fun sendKeyboardEvent(node: UINode, inputEvent: InputEvent) {
		val receiver = node.inputsReceiver ?: return

		when (inputEvent) {
			is KeyTyped -> receiver.onKeyTyped.set(inputEvent)
			is KeyDown -> receiver.onKeyDown.set(inputEvent)
			is KeyUp -> receiver.onKeyUp.set(inputEvent)
			else -> throw Error("Unsupported event: $inputEvent")
		}
	}

	private fun setupHierarchySelect(node: UINode?, position: Vector2d) {
		touchSelectController.action(node, {
			it.inputsReceiver?.onTouchFocused?.set(null)
		}, {
			it.inputsReceiver?.onTouchFocused?.set(TouchFocused(it, position))
		})
	}

	private fun setupHierarchyFocus(node: UINode?, position: Vector2d) {
		cursorFocusController.action(node, {
			it.inputsReceiver?.onMouseFocused?.set(null)
		}, {
			it.inputsReceiver?.onMouseFocused?.set(MouseFocused(it, position))
		})
	}

	private fun sendHierarchyPointerEvent(node: UINode, pointerEvent: PointerEvent) {
		sendPointerEvent(node, pointerEvent)

		node.parent?.let { parent ->
			sendHierarchyPointerEvent(parent, pointerEvent)
		}
	}

	private fun sendPointerEvent(node: UINode, pointerEvent: PointerEvent) {
		node.inputsReceiver?.let { receiver ->
			when (pointerEvent) {
				is MouseMove -> receiver.onMouseMove.set(pointerEvent)
				is MouseWheel -> receiver.onMouseWheel.set(pointerEvent)
				is TouchDown -> receiver.onTouchDown.set(pointerEvent)
				is TouchDragged -> receiver.onTouchDragged.set(pointerEvent)
				is TouchUp -> receiver.onTouchUp.set(pointerEvent)
				else -> throw Error("Unsupported event: $pointerEvent")
			}
		}
	}

	/**
	 * 	search how can catch event in tree
	 */
	private fun catchPointerEvent(node: UINode, event: PointerEvent): UINode? {
		val filter = node.touchFilterBuilder(node, event)

		if (filter.children) {
			node.childrenOrdered(true).forEach { child ->
				val result = catchPointerEvent(child, event)
				if (result != null) return result
			}
		}

		if (filter.self) {
			return node
		} else {
			return null
		}
	}

	fun setFocusIfEmpty(node: UINode) {
		node.inputs.onTouchFocused.value?.let { focus ->
			if (touchSelectController.items().contains(node)) return
			setupHierarchySelect(node, Vector2d.ZERO)
		}
	}
}
