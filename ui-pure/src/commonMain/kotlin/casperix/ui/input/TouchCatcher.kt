package casperix.ui.input

/**
 * 	Возможность "ловить" события тачпада / мыши.
 *
 * 	@param self -- искомый элемент принимает события
 * 	@param children --  детям открыта возможность "ловить" события
 *
 * 	@see NodeInputDispatcher
 */
data class TouchCatcher(val self: Boolean, val children: Boolean)

