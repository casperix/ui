package casperix.ui.layout

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.ui.type.LayoutSide

/**
 *	Располагает все элементы вдоль одно направления.
 *Направление вправо идет от левого края направо,
 * Направление вниз идет от верхноего края вверх
 * Направление вверх идет от нижнего края вниз
 * Направление влево идет от правого края влево
 */
data class LineLayout(
	val direction: LayoutSide
) : Layout {

	constructor(orientation: Orientation) : this(if (orientation == Orientation.VERTICAL) LayoutSide.BOTTOM else LayoutSide.RIGHT)

	override fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d {
		if (children.isEmpty()) return Vector2d.ZERO

		val bounds = children.map {
			it.getBound()
		}

		val left = direction.rotateCCW()
		val right = direction.rotateCW()

		val uAxis = direction.direction().toVector2d()
		val vAxis = right.direction().toVector2d()
		val uHorizontal = (direction == LayoutSide.LEFT || direction == LayoutSide.RIGHT)
		val vHorizontal = !uHorizontal


		val uStart = when (direction) {
			LayoutSide.RIGHT -> Vector2d.ZERO
			LayoutSide.LEFT -> viewSize
			LayoutSide.TOP -> viewSize.yAxis
			LayoutSide.BOTTOM -> viewSize.xAxis
		}
		var uPosition = uStart
		val vSize = viewSize.axisProjection(vHorizontal)

		val areas = List(children.size) { index ->
			val childrenBound = bounds[index]

			val uGap = if (index == 0) {
				getChildrenGap(null, childrenBound, direction)
			} else {
				getChildrenGap(bounds[index - 1], childrenBound, direction)
			}

			val vGapTop = childrenBound.border.gapFromSide(left)+ childrenBound.gap.gapFromSide(left)
			val vGapBottom  = childrenBound.border.gapFromSide(right)+ childrenBound.gap.gapFromSide(right)

			uPosition += uAxis * uGap

			val clientSize = childrenBound.clientSize.axisProjection(uHorizontal) + (vSize - vAxis.absoluteValue * (vGapTop + vGapBottom)).upper(Vector2d.ZERO)
			val clientCorner = uPosition + vAxis * vGapTop
			val clientArea = Box2d.byCorners(clientCorner, clientCorner + (uAxis + vAxis) * clientSize)

			uPosition += uAxis * clientSize

			clientArea
		}

		val gapSize = getChildrenGap(bounds.last(), null, direction)
		uPosition += uAxis * gapSize

		val uSize = (uPosition - uStart).absoluteValue
		val actualSize = uSize + vSize

//		if (autoCenter) areas = alignToCenter(viewSize, actualSize, areas)
		setAreas(children, areas)

		val maxBound = getMaxBound(bounds)
		val vPreferredSize = maxBound.placeSize.axisProjection(vHorizontal)
		val preferredSize = uSize + vPreferredSize
		return preferredSize
	}


}