package casperix.ui.layout

enum class Orientation {
	HORIZONTAL,
	VERTICAL;

	fun rotate(): Orientation {
		return when (this) {
			HORIZONTAL -> VERTICAL
			VERTICAL -> HORIZONTAL
		}
	}

}

