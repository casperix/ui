package casperix.ui.layout

import casperix.math.vector.Vector2d

/**
 * 	Разделяет доступное пространство на равные части для каждого элемента
 */
class UniformArrayLayout(val orientation: Orientation) : Layout {
	override fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d {
		val amount = children.size
		var position = Vector2d.ZERO
		if (orientation == Orientation.VERTICAL) {
			val childSize = Vector2d(viewSize.x, viewSize.y / amount)
			children.forEach {
				it.setArea(position, childSize)
				position += childSize.yAxis
			}
		} else {
			val childSize = Vector2d(viewSize.x / amount, viewSize.y)
			children.forEach {
				it.setArea(position, childSize)
				position += childSize.xAxis
			}
		}
		return viewSize
	}
}