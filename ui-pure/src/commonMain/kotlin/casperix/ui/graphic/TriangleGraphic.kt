package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.math.geometry.Triangle2d
import casperix.math.color.Color4d
import casperix.ui.engine.Drawer

/**
 * 	@see ShapeGraphic
 */
@Deprecated(message = "")
data class TriangleGraphic(
	val color: Color4d,
	val shape: Triangle2d,
) : Graphic {
	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		val globalShape = shape.convert { it + position }
		drawer.drawTriangle(globalShape, color)
	}
}