package casperix.ui.graphic

import casperix.math.color.Color4d
import casperix.math.geometry.Quad2d
import casperix.math.geometry.Triangle2d
import casperix.math.vector.Vector2d
import casperix.ui.engine.Drawer
import casperix.ui.engine.Shape

/**
 * 	Recommend use `ShapeBuilder` for create custom shape.
 * 	Also, your can use predefined constructor for simple shape.
 */
data class ShapeGraphic(
	val shape: Shape,
) : Graphic {
	constructor(color: Color4d, triangle: Triangle2d) : this(ShapeBuilder.triangle(triangle, ShapeStyle( color)))
	constructor(color: Color4d, quad: Quad2d) : this(ShapeBuilder.quad(quad, ShapeStyle(color)))

	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		shape.segments.forEach { segment ->
			segment.triangles.forEach { triangle ->
				val globalShape = triangle.convert { it + position }
				drawer.drawTriangle(globalShape, segment.material.color)
			}
		}
	}
}