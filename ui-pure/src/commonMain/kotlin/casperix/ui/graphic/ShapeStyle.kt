package casperix.ui.graphic

import casperix.math.color.Color4d

data class ShapeStyle(val bodyColor: Color4d = Color4d(1.0, 0.0, 0.0, 1.0), val borderColor: Color4d = Color4d(0.0, 0.0, 0.0, 1.0), val hasBody: Boolean = true, val borderThick: Double = 0.0) {
	val hasBorder = borderThick.isFinite() && borderThick > 0.0
}