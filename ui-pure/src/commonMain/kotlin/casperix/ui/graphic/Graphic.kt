package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.ui.engine.Drawer

interface Graphic {
	fun draw(drawer: Drawer, position: Vector2d, size: Vector2d)
}