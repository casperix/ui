package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.math.geometry.Quad2d
import casperix.ui.engine.Drawer
import casperix.ui.engine.Bitmap

data class QuadBitmapGraphic(
	val bitmap: Bitmap,
	val shape: Quad2d,
) : Graphic {

	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		val texture = bitmap
		val absoluteShape = shape.convert { it + position }

		drawer.drawTransformedBitmap(texture, absoluteShape)
	}
}