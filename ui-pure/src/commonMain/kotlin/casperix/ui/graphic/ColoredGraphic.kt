package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.math.color.Color4d
import casperix.math.vector.toQuad
import casperix.ui.engine.Drawer

data class ColoredGraphic(
	val color: Color4d,
) : Graphic {
	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		if (!size.greater(Vector2d.ZERO)) return
		drawer.drawQuad( Box2d.byDimension(position, size).toQuad(), color)
	}

}