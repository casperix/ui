package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.math.color.Color
import casperix.ui.engine.Drawer
import casperix.ui.font.Font
import casperix.ui.font.SymbolWidthMode
import casperix.ui.layout.Align

data class TextGraphic(
	val text: String,
	val font: Font,
	val color: Color,
	val wrap: Boolean = false,
	val horizontalAlign: Align = Align.CENTER,
	val verticalAlign: Align = Align.CENTER,
	val bold:Boolean = false,
	val italic:Boolean = false,
	val underline:Boolean = false,
	val symbolWidthMode: SymbolWidthMode = SymbolWidthMode.DEFAULT,
) : Graphic {

	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		drawer.drawText(this, position, size)
	}


}